const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(async (req, res, next) => {
	res.locals.user = req.user || null;
	next();
});

require('./core/api-routes')(app);

module.exports = app;
