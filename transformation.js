/* 
1. ALTER TABLE items ADD COLUMN oId TEXT; 
2. ALTER TABLE storeItems ADD COLUMN sOIId TEXT;



*/

const fs = require('fs');


const { stock_management, yeneStockDb } = require("./core/db/mysql");
const pushid = require("./utils/pushid");

/// 3
exports.addRandomIdToItemsTable = async () => {
  stock_management.query("SELECT * FROM items", (err, results) => {
    console.log(err);
    if (!err) {
      for (let index = 0; index < results.length; index++) {
        const element = results[index];
        stock_management.query(
          `UPDATE items SET oId="${pushid()}" WHERE id=${element.id}`,
          (err2, results2) => {}
        );
      }
    }
  });
};

/// 4

const storeSameItemId = []; // [[], []..]

exports.getCollisionDataFromStoreItems = async () => {
  try {
    const [results, m2] = await yeneStockDb.query(`SELECT si.*, item.name 
      FROM storeItems si
      INNER JOIN items item  
      ON item.id = si.stockItemId
      `);

    for (let index = 0; index < results.length; index++) {
      const ele = results[index];

      console.log(ele.name);
      const holdName = ele.name; // banana, salt..

      const [res2, m3] = await yeneStockDb.query(`SELECT si.*, item.name 
        FROM storeItems si
        INNER JOIN items item  
        ON item.id = si.stockItemId
        WHERE item.name = "${holdName}"
      `);

      res2.forEach((item) => {
        if (item.id != ele.id) {
          storeSameItemId.push(item);
        }
      });
    }
  } catch (error) {
    console.log(error);
  }

  
  
  // console.log(storeSameItemId);
  fs.writeFileSync('collision-data.json', JSON.stringify(storeSameItemId));
};



/// 5
exports.storeStockOIdToItemStore = async () => {
  const [results, m2] = await yeneStockDb.query(`
      SELECT si.*, 
      item.name,
      item.oId
      FROM storeItems si
      INNER JOIN items item  
      ON item.id = si.stockItemId
      `);
      console.log(results);
}