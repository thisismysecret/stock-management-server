exports.optimizeQueryPagination = (page, l) => {
	let limits = l;
	if (!limits || limits === '' || limits.length === 0) {
		limits = 7;
	}
	const skip = (page * limits) - limits;
	return { page, limits: Number(limits), skip };
};
