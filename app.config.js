
exports.APP = {
	PORT: 3000,
	HOST: 'localhost',
	URL: '',
};

exports.CROSS_DOMAIN = {
	allowedOrigins: '*',
	allowedReferer: '',
};

exports.COOKIE_SECRET = 'thisismysecret1077';

exports.INFO = {
	name: '',
	version: '1.0',
	author: '',
	site: exports.APP.URL,
	powered: ['Vue', 'Nodejs', 'Express'],
};
