/* eslint-disable no-param-reassign */
/* eslint-disable no-restricted-syntax */
// exports.phoneFormat = (phone) => {
// 	const phoneArray = [...phone];
// 	let str = '';
// 	phoneArray.forEach((item, index) => {
// 		if (index === 1 || index === 2 || index === 3 || index === 4 || index === 5 || index === 6 || index === 7 || index === 8 || index === 9) {
// 			str += '*';
// 		} else {
// 			str += item;
// 		}
// 	});
// 	return str;
// };



/**
	* replaceAt('13252864200', 3, '****')
 */
exports.replaceAt = (string, index, replacement) => string.substr(0, index) + replacement + string.substr(index + replacement.length);

/**
 * this.phoneFormat('13267229922', {})
*/
exports.phoneFormat = (phone, { length = 3 }) => this.replaceAt(phone.toString(), 3, '*'.repeat(length));
// const phoneArray = [...phone];
// let str = '';
// phoneArray.forEach((item, index) => {
// 	if (index === 3 || index === 4 || index === 5 || index === 6 || index === 7 || index === 8) {
// 		str += '*';
// 	} else {
// 		str += item;
// 	}
// });
// return str;


exports.convertLetterToNumber = (txt, literal, join) => {
	// eslint-disable-next-line no-param-reassign
	if (this.isDef(txt)) {
		if (!literal) txt = txt.toLowerCase();
		return txt.split('').map(c => 'abcdefghijklmnopqrstuvwxyz'.indexOf(c) + 1 || (literal ? c : '')).join(join || '');
	}
	return '';
};

// console.log(this.convertLetterToNumber('hello', null, ' '));

exports.convertNumberToLetter = num => String.fromCharCode(num + 64);

// '8/5/12/12/15'.split('/').forEach((v) => {
// 	console.log(this.convertNumberToLetter(Number(v)));
// });

exports.currentDate = () => Date.now();

exports.padLeftZero = str => (`00${str}`).substr(str.length);


/**
 *
	new Date().getTime()
	let date = new Date(1554647023445)
	formatDate(date, 'yyyy-MM-dd hh:mm')
	=> 2019-04-07 22:23
*/
exports.formatDate = (date, fmt) => {
	if (/(y+)/.test(fmt)) {
		fmt = fmt.replace(
			RegExp.$1,
			(`${date.getFullYear()}`).substr(4 - RegExp.$1.length),
		);
	}
	const o = {
		'M+': date.getMonth() + 1,
		'd+': date.getDate(),
		'h+': date.getHours(),
		'm+': date.getMinutes(),
		's+': date.getSeconds(),
	};
	for (const k in o) {
		if (new RegExp(`(${k})`).test(fmt)) {
			const str = `${o[k]}`;
			fmt = fmt.replace(
				RegExp.$1,
				RegExp.$1.length === 1 ? str : this.padLeftZero(str),
			);
		}
	}
	return fmt;
};

exports.randomNumber = () => (new Date().getUTCMilliseconds().toString() + new Date().getTime().toString()).toString();

exports.toNumber = number => Number(number);


/**
 * timeAgo(new Date(Number('1580344653925'))) // must be number
 * timeAgo(new Date('2020-01-30T00:51:14.172Z'))
 * const aDay = 24 * 60 * 60 * 1000;
console.log(timeAgo(new Date(Date.now() - aDay)));
 console.log(timeAgo(new Date(Date.now() - aDay * 2)));
*/
exports.timeAgo = (date) => {
	const seconds = Math.floor((new Date() - date) / 1000);

	let interval = Math.floor(seconds / 31536000);

	if (interval > 1) {
		return `${interval} years`;
	}
	interval = Math.floor(seconds / 2592000);
	if (interval > 1) {
		return `${interval} months`;
	}
	interval = Math.floor(seconds / 86400);
	if (interval > 1) {
		return `${interval} days`;
	}
	interval = Math.floor(seconds / 3600);
	if (interval > 1) {
		return `${interval} hours`;
	}
	interval = Math.floor(seconds / 60);
	if (interval > 1) {
		return `${interval} minutes`;
	}
	return `${Math.floor(seconds)} seconds`;
};

exports.randomArray = (n, l) => {
	const rnd = [];
	for (let i = 0; i < n; i += 1) {
		rnd.push(Math.floor(Math.random() * l));
	}
	return rnd;
};


exports.removeDuplicates = (myArr, prop) => myArr.filter((obj, pos, arr) => arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos);


exports.isDef = value => value !== undefined && value !== null;

exports.isSameDate = (dateA, dateB) => dateA.toISOString() === dateB.toISOString();

exports.is = (val, type) => ![, null].includes(val) && val.constructor === type;

/**
 * Run every second
	setInterval(updateCountdown, 1000);
 */
// Update countdown time
function updateCountdown() {
	const currentYear = new Date().getFullYear();

	const newYearTime = new Date(`January 01 ${currentYear + 1} 00:00:00`);
	const currentTime = new Date();
	const diff = newYearTime - currentTime;

	const d = Math.floor(diff / 1000 / 60 / 60 / 24);
	const h = Math.floor(diff / 1000 / 60 / 60) % 24;
	const m = Math.floor(diff / 1000 / 60) % 60;
	const s = Math.floor(diff / 1000) % 60;

	console.log(`${d} days ${h < 10 ? `0${h}` : h} hours ${m < 10 ? `0${m}` : m} minutes ${s < 10 ? `0${s}` : s} seconds`);
}

// eslint-disable-next-line no-sparse-arrays
const is = (type, val) => ![, null].includes(val) && val.constructor === type;


exports.ratingCalculator = (data, label) => {
	let large = 0;
	const one = data.filter(item => item[label] === 1).length;
	const two = data.filter(item => item[label] === 2).length;
	const three = data.filter(item => item[label] === 3).length;
	const four = data.filter(item => item[label] === 4).length;
	const five = data.filter(item => item[label] === 5).length;

	large = Math.max.apply(null, [one, two, three, four, five]); // large rating num
	const ratings = [one, two, three, four, five].reduce((a, b) => a + b, 0); // sum of ratings

	return {
		rates: ((5 * five + 4 * four + 3 * three + 2 * two + 1 * one) / (five + four + three + two + one)).toFixed(2),
		weight: {
			one: this.toNumber((one / ratings).toFixed(2)), // length / sum
			two: this.toNumber((two / ratings).toFixed(2)),
			three: this.toNumber((three / ratings).toFixed(2)),
			four: this.toNumber((four / ratings).toFixed(2)),
			five: this.toNumber((five / ratings).toFixed(2)),
		},
		ratings,
	};
};
