require('dotenv').config()
var compression = require('compression')
const express = require('express');
require('express-async-errors');
const helmet = require('helmet');
// const cors = require('cors');
const errorHandlers = require('./core/errorHandlers');
const apiV1 = require('./app-v1');
const apiV2 = require('./app-v2');

var fs = require('fs');
var util = require('util');
const logger = require('morgan');
var path = require('path');
const winston = require('winston');
 
const logger2 = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  defaultMeta: { service: 'user-service' },
  transports: [
    //
    // - Write all logs with level `error` and below to `error.log`
    // - Write all logs with level `info` and below to `combined.log`
    //
    new winston.transports.File({ filename: 'error.log', level: 'error' }),
    new winston.transports.File({ filename: 'combined.log' }),
  ],
});

var log_file = fs.createWriteStream(__dirname + '/debug.log', {flags : 'w'});
var log_stdout = process.stdout;

console.log = function(d) { //
  // log_file.write(util.format(d) + '\n');
  // log_stdout.write(util.format(d) + '\n');
	logger2.log({level: 'info',message: util.format(d) + '\n'});
};


console.log(process.env.DB_HOST);
console.log(process.env.DB_USER);
console.log(process.env.DB_PASSWORD);
console.log(process.env.DB_DB);
// const {addRandomIdToItemsTable, getCollisionDataFromStoreItems, storeStockOIdToItemStore} = require('./transformation');


// 1
// addRandomIdToItemsTable();

// 2
// getCollisionDataFromStoreItems();

// 3
// storeStockOIdToItemStore();


const app = express();


app.use(logger('common', {
  stream: fs.createWriteStream(path.join(__dirname, '/debug.log'), { flags: 'a' })
}))


// yeneStockDb.authenticate()
// 	.then(() => {
// 		console.log('yeneStockDb Connection has been established successfully.');
// 	})
// 	.catch((err) => {
// 		console.error('Unable to connect to the database:', err);
// 	});

// app.use(cors());

/**
 * Helmet helps to secure Express apps by setting various HTTP headers.
 */
app.use(helmet());

app.use(compression())


/**
 * Get NODE_ENV from environment and store in Express.
 */
app.set('env', 'production');

/**
 * When running Express app behind a proxy we need to detect client IP address correctly.
 * For NGINX the following must be configured 'proxy_set_header X-Forwarded-For $remote_addr;'
 * @link http://expressjs.com/en/guide/behind-proxies.html
 */
app.set('trust proxy', true);


app.use('/api', apiV1);
app.use('/api/v2', apiV2);

app.get('/hi', (req, res) => {
	res.status(200).send({
		status: 200,
		Msg: 'Hi there',
	});
})


// If that above routes didnt work, we 404 them and forward to error handler
app.use(errorHandlers.notFound);


// Otherwise this was a really bad error we didn't expect! Shoot eh
if (app.get('env') === 'development') {
  /* Development Error Handler - Prints stack trace */
  app.use(errorHandlers.developmentErrors);
}

// production error handler
app.use(errorHandlers.productionErrors);


app.set('port', 3000);
const server = app.listen(app.get('port'), () => {
	console.log(`Express running → PORT ${server.address().port}`);
	console.log(`NODE_ENV=${process.env.NODE_ENV}`);
});
