const Utils = require('../core/processor');

module.exports = {
	isAuthenticated(req, res, next) {
		// console.log(req.session);

		if (req.isAuthenticated()) {
			return next();
		}
		return Utils.ErrMsg(res, { msg: 'please login first', Code: '--1' });
	},
//   confirmOwner: function(req, res, product){
//     if (!product.user.equals(req.user)){
//         res.json({"o":"nono"});
//         console.log('You must own a store in order to edit it!');
//     }
// }
};
