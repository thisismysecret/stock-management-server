// module.exports = {
//   asyncErrorHandler: fn => (req, res, next) => {
//     Promise.resolve(fn(req, res, next)).catch(next);
//   }
// };

//  handle promise error (catch any error (async await))
// eslint-disable-next-line func-names
exports.catchErrors = fn => function (req, res, next) {
	return fn(req, res, next).catch(next);
};
