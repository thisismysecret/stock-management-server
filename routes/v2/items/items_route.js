/* @api /api/items */
const express = require('express');
const fs = require("fs");
const excelToJson = require("convert-excel-to-json");
const router = express.Router();
const { catchErrors } = require('../../../middleware/index');
const { isAuthenticated } = require('../../../middleware/isAuthenticated');
const {stock_management} = require('../../../core/db/mysql');
const Utils = require('../../../core/processor')
const ItemModelSql = require('../../../models/item_model');
const BranchLocalSql = require('../../../models/branch_model');
const { optimizeQueryPagination } = require('../../../Global/routeHelpers');
const branch_model = require('../../../models/branch_model');
const { deductStockItem } = require('../../v1/item_helper');
const pushid = require('../../../utils/pushid');

const itemTable = 'items';

router.post('/createItemTable', (req, res) => {
	const sql_q = `CREATE TABLE ${itemTable}
  (
    ${ItemModelSql.id} INTEGER AUTO_INCREMENT PRIMARY KEY,
    ${ItemModelSql.originalId} TEXT,
    ${ItemModelSql.name} TEXT,
    ${ItemModelSql.brand} TEXT,
    ${ItemModelSql.image} TEXT,
    ${ItemModelSql.code} TEXT,
    ${ItemModelSql.invoiceNo} TEXT,
    ${ItemModelSql.invoiceDate} TEXT,
    ${ItemModelSql.unitPrice} REAL(10,2),
    ${ItemModelSql.maxPrice} REAL(10,2),
    ${ItemModelSql.minPrice} REAL(10,2),
    ${ItemModelSql.qty} INTEGER,
    ${ItemModelSql.adminId} INTEGER,
    ${ItemModelSql.partNo} TEXT,
    ${ItemModelSql.barcode} TEXT,
    ${ItemModelSql.branchId} INTEGER,
    ${ItemModelSql.createdAt} TEXT,
    ${ItemModelSql.description} TEXT,
    ${ItemModelSql.expireDate} TEXT,
    ${ItemModelSql.expireRemindId} INTEGER,
    ${ItemModelSql.groupp} TEXT,
    ${ItemModelSql.location} TEXT,
    ${ItemModelSql.manufacturer} TEXT,
    ${ItemModelSql.manufacturerDate} TEXT,
    ${ItemModelSql.mark} TEXT,
    ${ItemModelSql.measurement} TEXT,
    ${ItemModelSql.purchasePrice} REAL,
    ${ItemModelSql.status} TEXT 
  )`;
    stock_management.query(sql_q, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {
      msg: err.code,
      err: err
    });
    if (results && results.length === 0) return Utils.ErrMsg(res, 
      { 
      msg: 'Failed to create',  
      err: err
    });
		return Utils.SuccessMsg(res, { });
	});
});

router.post('/excel_to_app', async (req, res) => {

  try {
    
    const result = excelToJson({
      source: fs.readFileSync("yene_stock_test_accessories.xlsx"), // fs.readFileSync return a Buffer
      // columnToKey: {
      //   A: "id",
      //   B: ItemModelSql.name,
      //   C: ItemModelSql.partNo,
      //   D: ItemModelSql.qty,
      //   E: ItemModelSql.unitPrice,
      // },
      //   header:{
      // Is the number of rows that will be skipped and will not be present at our result object. Counting from top to bottom
      //     rows: 1 // 2, 3, 4, etc.
      // }
    });
  
    const fields = [];
  
    function getKeyByValue(v) {
      return fields.filter((x) => x.v === v)[0].k;
    }
    const sheet1 = result.Sheet1;
  
    if(sheet1[0]) {
      const sheetsFieldKey = Object.keys(sheet1[0]);
      const sheetsFieldValue = Object.values(sheet1[0]);
      
      for (let i = 0; i < sheetsFieldKey.length; i++) {
        const key = sheetsFieldKey[i];
        const value = sheetsFieldValue[i];
    
    
          fields.push({
            v: value,
            k: key
          });
        
      }
    }
  
    const itemModelList = [];
    const itemModelList2 = [];
  
    for (let i = 1; i < result.Sheet1.length; i++) {
      const element = result.Sheet1[i];
      // console.error('tag', element[getKeyByValue(ItemModelSql.name)])
    
      itemModelList.push({
        [ItemModelSql.name]: element[getKeyByValue(ItemModelSql.name)],
        [ItemModelSql.image]: "",
        [ItemModelSql.unitPrice]: element[getKeyByValue(ItemModelSql.unitPrice)], // set
        [ItemModelSql.maxPrice]: 0,
        [ItemModelSql.minPrice]: 0,
        [ItemModelSql.qty]: element[getKeyByValue(ItemModelSql.qty)],
        [ItemModelSql.adminId]: 1,
        [ItemModelSql.partNo]: element[getKeyByValue(ItemModelSql.partNo)],
        [ItemModelSql.barcode]: "",
        [ItemModelSql.branchId]: 1,
        [ItemModelSql.brand]: 'ACCESSORIES',
        [ItemModelSql.createdAt]: new Date(Date.now()),
        [ItemModelSql.description]: "",
        [ItemModelSql.groupp]: "",
        [ItemModelSql.location]: "",
        [ItemModelSql.manufacturer]: "",
        [ItemModelSql.manufacturerDate]: "",
        [ItemModelSql.mark]: "",
        [ItemModelSql.measurement]: "",
        [ItemModelSql.status]: "0",
        [ItemModelSql.expireDate]: "",
        [ItemModelSql.expireRemindId]: "",
      });

      itemModelList2.push([
        element[getKeyByValue(ItemModelSql.name)],
        "",
        element[getKeyByValue(ItemModelSql.unitPrice)], // set
        0,
        0,
        0, // qty
        1,
        element[getKeyByValue(ItemModelSql.partNo)],
        "",
        3, // branch
        "ACCESSORIES", // brand
        new Date(Date.now()),
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "0",
        "",
        "",
      ]);

    }
    const sql = `INSERT INTO ${itemTable} 
  (
    ${ItemModelSql.name},
    ${ItemModelSql.image},
    ${ItemModelSql.unitPrice},
    ${ItemModelSql.maxPrice},
    ${ItemModelSql.minPrice},
    ${ItemModelSql.qty},
    ${ItemModelSql.adminId},
    ${ItemModelSql.partNo},
    ${ItemModelSql.barcode},
    ${ItemModelSql.branchId},
    ${ItemModelSql.brand},
    ${ItemModelSql.createdAt},
    ${ItemModelSql.description},
    ${ItemModelSql.groupp},
    ${ItemModelSql.location},
    ${ItemModelSql.manufacturer},
    ${ItemModelSql.manufacturerDate},
    ${ItemModelSql.mark},
    ${ItemModelSql.measurement},
    ${ItemModelSql.status},
    ${ItemModelSql.expireDate},
    ${ItemModelSql.expireRemindId}
  )
  VALUES 
  ?
  `;
    stock_management.query(sql,[itemModelList2], (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, { });
	});
  }
   catch (error) {
      console.log(error);
   }
})


router.post('/addItemToDB', async (req, res) => {
  const {itemModel} = req.body;
	const sql = `INSERT INTO ${itemTable} 
  (
    ${ItemModelSql.originalId},
    ${ItemModelSql.name},
    ${ItemModelSql.image},
    ${ItemModelSql.unitPrice},
    ${ItemModelSql.maxPrice},
    ${ItemModelSql.minPrice},
    ${ItemModelSql.purchasePrice},
    ${ItemModelSql.code},
    ${ItemModelSql.invoiceNo},
    ${ItemModelSql.invoiceDate},
    ${ItemModelSql.qty},
    ${ItemModelSql.adminId},
    ${ItemModelSql.partNo},
    ${ItemModelSql.barcode},
    ${ItemModelSql.branchId},
    ${ItemModelSql.brand},
    ${ItemModelSql.createdAt},
    ${ItemModelSql.description},
    ${ItemModelSql.groupp},
    ${ItemModelSql.location},
    ${ItemModelSql.manufacturer},
    ${ItemModelSql.manufacturerDate},
    ${ItemModelSql.mark},
    ${ItemModelSql.measurement},
    ${ItemModelSql.status},
    ${ItemModelSql.expireDate},
    ${ItemModelSql.expireRemindId}
  )
  VALUES 
  (
    "${pushid()}",
    "${itemModel.name}",
    "${itemModel.image}",
    "${itemModel.unitPrice || 0}",
    "${itemModel.maxPrice}",
    "${itemModel.minPrice}",
    "${itemModel.purchasePrice || 0}",
    "${itemModel.code}",
    "${itemModel.invoiceNo}",
    "${itemModel.invoiceDate}",
    "${itemModel.qty}",
    "${itemModel.adminId}",
    "${itemModel.partNo}",
    "${itemModel.barcode}",
    "${itemModel.branchId}",
    "${itemModel.brand}",
    "${itemModel.createdAt}",
    "${itemModel.description}",
    "${itemModel.groupp}",
    "${itemModel.location}",
    "${itemModel.manufacturer}",
    "${itemModel.manufacturerDate}",
    "${itemModel.mark}",
    "${itemModel.measurement}",
    "${itemModel.status}",
    "${itemModel.expireDate || ''}",
    "${itemModel.expireRemindId || 9999}"
  )
  `;
    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, { });
	});
});

/* 
[
  {
    t: 'branch',
    v: '= 1'
  },
  {
    t: 'qty',
    v: '<= 2'
  },
  {
    t: 'mark',
    v: '= SKF'
  }
]

WHERE 

*/

router.post('/getPaginatedItems',  (req, res) => {
  let {page, sortBy, filterBy,} = req.body;
  console.log(req.body);

  let sql = '';
  let where = '';

  // advance filter
  if(filterBy && filterBy.length > 0) {
    where = `WHERE ${filterBy.map((v) => `i.${v.technicalName} ${v.value}`).join(' AND ')}`
  }

  sortBy = sortBy && sortBy.technicalName ? sortBy : {technicalName: 'createdAt', value: 'DESC'};

  const {limits, skip} = optimizeQueryPagination(page)


  sql = `
    SELECT i.*, b.${branch_model.name} AS "${ItemModelSql.branchName}", b.type AS "${ItemModelSql.branchType}" FROM ${itemTable} i
    LEFT JOIN branch b ON i.${ItemModelSql.branchId} = b.${ItemModelSql.id}
    ${where}
    ORDER BY i.${sortBy.technicalName} ${sortBy.value} LIMIT ${limits} OFFSET ${skip}
  `;

  console.log(sql);
  stock_management.query(sql, (err, results) => {
    console.log(err);
  if (err) return Utils.ErrMsg(res, {msg: err.code});
  return Utils.SuccessMsg(res, {data: results });
});
// const [results, metadata] = await yeneStockDb.query(sql);
// return Utils.SuccessMsg(res, {data: results,msg: metadata });

});
router.post('/getPaginatedSearchItems', (req, res) => {
  const {q, page, filterBy} = req.body;
  const {limits, skip} = optimizeQueryPagination(page)
  console.log(req.body);

  let where = `WHERE (i.${ItemModelSql.name} LIKE '%${q}%' 
  OR
  i.${ItemModelSql.partNo} LIKE '%${q}%' 
  OR
  i.${ItemModelSql.barcode} LIKE '%${q}%') `;

  // advance filter
  if(filterBy && filterBy.length > 0) {
    where += ` AND ${filterBy.map((v) => `i.${v.technicalName} ${v.value}`).join(' AND ')}`
  }

  const sql = `SELECT i.*, b.${branch_model.name} AS "${ItemModelSql.branchName}", b.type AS "${ItemModelSql.branchType}" 
  FROM ${itemTable} i
  LEFT JOIN branch b ON i.${ItemModelSql.branchId} = b.${ItemModelSql.id}  
  ${where}
  LIMIT ${limits} OFFSET ${skip}`;

    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});

router.post('/getAllItems', (req, res) => {
  const sql = `SELECT * FROM ${itemTable}`;

    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});

router.post('/getItemIn', (req, res) => {
  const {ids} = req.body;
  console.log(ids);
  // const sql = `SELECT * FROM ${itemTable} WHERE ${ItemModelSql.id} IN (${ids.join(', ')})`;
  const sql = `
      SELECT i.*, b.${branch_model.name} AS "${ItemModelSql.branchName}", b.type AS "${ItemModelSql.branchType}" FROM ${itemTable} i
      LEFT JOIN ${BranchLocalSql.branchTable} b ON i.${ItemModelSql.branchId} = b.${ItemModelSql.id} 
      WHERE i.${ItemModelSql.id} IN (${ids.join(', ')})
      
     `;
    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});

router.post('/updateItem', (req, res) => {
  const {id, key, value} = req.body;
	const sql = `UPDATE ${itemTable} SET ${key} = ${value} WHERE ${ItemModelSql.id} = ${id}`;
    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});


router.post('/deductItem', async (req, res) => {
  const {id, key, value} = req.body;
  // const sql = `UPDATE ${itemTable} SET ${key} = GREATEST(${key} - ${value}, 0) WHERE ${ItemModelSql.id} = ${id}`;
  
  deductStockItem(id, key, value).then((results) => {
    return Utils.SuccessMsg(res, {data: results });
  }).catch((err) => Utils.ErrMsg(res, {msg: err.code}));

  //   stock_management.query(sql, (err, results) => {
  //     console.log(err);
	// 	if (err) return ;
		
	// });
});

router.post('/addQtyItem', (req, res) => {
  const {id, key, value} = req.body;
	const sql = `UPDATE ${itemTable} SET ${key} = GREATEST(${key} + ${value}, ${value}) WHERE ${ItemModelSql.id} = ${id}`;
    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});
router.post('/updateAllItem', (req, res) => {
  const {id, itemModel} = req.body;
  const sql = `UPDATE ${itemTable} SET 
  ${ItemModelSql.name} = "${itemModel.name}",
  ${ItemModelSql.image} = "${itemModel.image}",
  ${ItemModelSql.unitPrice} = "${itemModel.unitPrice || 0}",
  ${ItemModelSql.maxPrice} = "${itemModel.maxPrice}",
  ${ItemModelSql.minPrice} = "${itemModel.minPrice}",
  ${ItemModelSql.purchasePrice} = "${itemModel.purchasePrice || 0}",
  ${ItemModelSql.code} = "${itemModel.code}",
  ${ItemModelSql.invoiceNo} = "${itemModel.invoiceNo}",
  ${ItemModelSql.invoiceDate} = "${itemModel.invoiceDate}",
  ${ItemModelSql.qty} = "${itemModel.qty}",
  ${ItemModelSql.partNo} = "${itemModel.partNo}",
  ${ItemModelSql.barcode} = "${itemModel.barcode}",
  ${ItemModelSql.branchId} = "${itemModel.branchId}",
  ${ItemModelSql.brand} = "${itemModel.brand}",
  ${ItemModelSql.description} = "${itemModel.description}",
  ${ItemModelSql.groupp} = "${itemModel.groupp}",
  ${ItemModelSql.location} = "${itemModel.location}",
  ${ItemModelSql.manufacturer} = "${itemModel.manufacturer}",
  ${ItemModelSql.manufacturerDate} = "${itemModel.manufacturerDate}",
  ${ItemModelSql.mark} = "${itemModel.mark}",
  ${ItemModelSql.measurement} = "${itemModel.measurement}",
  ${ItemModelSql.status} = "${itemModel.status}" 
  WHERE ${ItemModelSql.id} = ${id}`;
    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});

router.post('/deleteItem', (req, res) => {
  const {id} = req.body;
	const sql = `DELETE FROM ${itemTable} WHERE ${ItemModelSql.id} = ${id}`;
    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});
router.post('/itemsCount', (req, res) => {
	const sql = `SELECT COUNT(*) FROM ${itemTable}`;
    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});
router.post('/getLowInStockCount', (req, res) => {
  const {lowInStock = 0}= req.body;
	const sql = `SELECT COUNT(*) AS stockCount FROM ${itemTable} WHERE ${ItemModelSql.qty} <= ${lowInStock}`;
    stock_management.query(sql, (err, results) => {
      console.log(err);
    if (err) return Utils.ErrMsg(res, {msg: err.code});
    console.log(results);
		return Utils.SuccessMsg(res, {data: results[0]['stockCount'] });
	});
});

router.post('/getPaginatedLowInStockItems', (req, res) => {
  const {page, lowInStock = 0} = req.body;
  const {limits, skip} = optimizeQueryPagination(page)
  const sql = `SELECT i.*, b.${branch_model.name} AS "${ItemModelSql.branchName}", b.type AS "${ItemModelSql.branchType}" FROM ${itemTable} i
  LEFT JOIN ${BranchLocalSql.branchTable} b ON i.${ItemModelSql.branchId} = b.${ItemModelSql.id} 
  WHERE ${ItemModelSql.qty} <= ${lowInStock} ORDER BY ${ItemModelSql.createdAt} DESC LIMIT ${limits} OFFSET ${skip}`;
    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});

router.post('/transferItemFromBranchToBranch', (req, res) => {
  const {branchId, items  } = req.body;
  console.log(branchId, items);
  /**
   * @items
   *   {
      id: 1,
      qty: 2,
      all: false // if all is true ignore qty
    }
  */
    
 let sql = [];
 let lengths = [];
  if(items && items.length){
    for (let index = 0; index < items.length; index++) {
      const element = items[index];
      if(element.all === true){
        sql.push(`UPDATE ${itemTable} SET ${ItemModelSql.branchId} = ${branchId} WHERE ${ItemModelSql.id} = ${element.id}`);
        lengths.push(index)
      } else {
        // sql.push(`UPDATE FROM ${itemTable} WHERE ${ItemModelSql.id} = ${element.id} SET ${ItemModelSql.branchId} = ${branchId}`);
      }
      
    }
  }
    stock_management.query(sql.join(';'),lengths, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });
  
  
});

router.post('/transferFromStockToStore', (req, res) => {
  let {item, branchId, qty,adminId} = req.body;
  console.log(req.body);
  const isItemExistInStoreSql =
      `SELECT * from ${ItemModelSql.storeItemTable} WHERE ${ItemModelSql.stockOriginalItemId} = "${item[ItemModelSql.originalId]}" AND ${ItemModelSql.branchId} = ${branchId} `;

      stock_management.query(isItemExistInStoreSql, (err1, results1) => {
        console.log(err1);
        if (err1) return Utils.ErrMsg(res, {msg: err1.code});
        console.log('results1', results1);
        if(results1 && results1.length > 0) {
          // the item is exist
          const sql = ` 
          UPDATE ${ItemModelSql.storeItemTable} 
          SET ${ItemModelSql.qty} = GREATEST(${ItemModelSql.qty} + ${qty}, ${qty}) 
          WHERE ${ItemModelSql.stockOriginalItemId} = "${item[ItemModelSql.originalId]}" AND ${ItemModelSql.branchId} = ${branchId}
          `;
          stock_management.query(sql, (err3, results3) => {
            if (err3) return Utils.ErrMsg(res, {msg: err3.code});
            const sql2 = `UPDATE ${itemTable} SET ${ItemModelSql.qty} = GREATEST(${ItemModelSql.qty} - ${qty}, 0) 
            WHERE ${ItemModelSql.id} = ${item.id}`;
            stock_management.query(sql2, (err4, results4) => {
              if (err4) return Utils.ErrMsg(res, {msg: err4.code});
              return Utils.SuccessMsg(res, { });
            })
          })

        } else {
          // create new

          item.status = '0';
          item.qty = qty;
          item.location = '';
          const createdAt = new Date(Date.now()).toString();
          const sqll = `
            INSERT INTO ${ItemModelSql.storeItemTable} 
            (
              ${ItemModelSql.stockItemId},
              ${ItemModelSql.stockOriginalItemId},
              ${ItemModelSql.qty},
              ${ItemModelSql.adminId},
              ${ItemModelSql.branchId},
              ${ItemModelSql.createdAt},
              ${ItemModelSql.location},
              ${ItemModelSql.status}
            )
            VALUES 
            (
              "${item.id}",
              "${item[ItemModelSql.originalId]}",
              "${item.qty}",
              "${adminId}",
              "${branchId}",
              "${createdAt}",
              "${item.location}",
              "${item.status}"
            )
            `;

            stock_management.query(sqll, (err, results) => {
              console.log(err);
            if (err) return Utils.ErrMsg(res, {msg: err.code});
            const sql2 = `UPDATE ${itemTable} SET ${ItemModelSql.qty} = GREATEST(${ItemModelSql.qty} - ${qty}, 0) WHERE ${ItemModelSql.id} = ${item.id}`;
            stock_management.query(sql2, (err3, results3) => {
              if (err3) return Utils.ErrMsg(res, {msg: err3.code});
              return Utils.SuccessMsg(res, { });
            })
            })
        }
      })
})

/// get to be expire items
router.post('/toBeExpireDateItemsCount', (req, res) => {
  const {date} = req.body;
  
      const sql = `
      SELECT COUNT(*) AS expireCount FROM ${itemTable}
      WHERE ${ItemModelSql.expireDate} != "" AND ${ItemModelSql.expireDate} <= "${date}"
    `;
    stock_management.query(sql, (err3, results3) => {
      console.log(err3);
      if (err3) return Utils.ErrMsg(res, {msg: err3.code});
      return Utils.SuccessMsg(res, {data: results3[0]['expireCount'] });
    })

})

/// get to be expire items
router.post('/toBeExpireDateItemsPagination', (req, res) => {
  const {date, page} = req.body;

  const {limits, skip} = optimizeQueryPagination(page)

  
      const sql = `
      
      SELECT i.*, b.${branch_model.name} AS "${ItemModelSql.branchName}", b.type AS "${ItemModelSql.branchType}" FROM ${itemTable} i
      LEFT JOIN ${BranchLocalSql.branchTable} b ON i.${ItemModelSql.branchId} = b.${ItemModelSql.id} 
      WHERE ${ItemModelSql.expireDate} != "" AND ${ItemModelSql.expireDate} <= "${date}"
      ORDER BY ${ItemModelSql.expireDate} ASC LIMIT ${limits} OFFSET ${skip}
    `;
    stock_management.query(sql, (err3, results3) => {
      if (err3) return Utils.ErrMsg(res, {msg: err3.code});
      return Utils.SuccessMsg(res, {data: results3});
    })

})

module.exports = router;