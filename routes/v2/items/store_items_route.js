/* @api /api/store_items */
const express = require('express');

const router = express.Router();
const { catchErrors } = require('../../../middleware/index');
const { isAuthenticated } = require('../../../middleware/isAuthenticated');
const {stock_management,yeneStockDb} = require('../../../core/db/mysql');
const Utils = require('../../../core/processor')
const ItemModelSql = require('../../../models/item_model');
const { optimizeQueryPagination } = require('../../../Global/routeHelpers');
const branch_model = require('../../../models/branch_model');
const { deductStoreItem } = require('../../v1/item_helper');
const { itemTable } = require('../../../models/item_model');
const { DATETIME } = require('mysql2/lib/constants/types');

const storeItem = 'storeItems';

const storeItemsCol = [ItemModelSql.createdAt, ItemModelSql.branchId, ItemModelSql.location, ItemModelSql.status,ItemModelSql.qty];

router.post('/createStoreItemTable', (req, res) => {
	const sql_q = `CREATE TABLE ${storeItem}
  (
    ${ItemModelSql.id} INTEGER AUTO_INCREMENT PRIMARY KEY ,
    ${ItemModelSql.stockItemId} INTEGER,
    ${ItemModelSql.stockOriginalItemId} TEXT,
    ${ItemModelSql.qty} INTEGER,
    ${ItemModelSql.adminId} INTEGER,
    ${ItemModelSql.branchId} INTEGER,
    ${ItemModelSql.createdAt} TEXT,
    ${ItemModelSql.location} TEXT,
    ${ItemModelSql.status} TEXT 
  )`;
    stock_management.query(sql_q, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
    if (results && results.length === 0) return Utils.ErrMsg(res, { msg: 'Failed to create' });
		return Utils.SuccessMsg(res, { });
	});
});

router.post('/addItemToDB', async (req, res) => {
  const {itemModelList,adminId,
    branchId,
    createdAt} = req.body;
  const values = [];
    console.log(req.body);

  const toBeUpdatedItems = [];


  for (let index = 0; index < itemModelList.length; index++) {
  const element = itemModelList[index];

  const isItemExistInStoreSql =
  `SELECT * from ${ItemModelSql.storeItemTable} WHERE ${ItemModelSql.stockOriginalItemId} = "${element.originalId}" AND ${ItemModelSql.branchId} = ${branchId} `;

  console.log(isItemExistInStoreSql);

  // stock_management(req).query(isItemExistInStoreSql, (err1, results1) => {
    const [results1, metadata] = await  yeneStockDb.query(isItemExistInStoreSql);
    // if (err1) {
    //   console.log('err1', err1);
    //   return Utils.ErrMsg(res, {msg: err1.code})
    // };
    console.log('results1', results1);
    if(results1 && results1.length > 0) {
      // the item is exist
      toBeUpdatedItems.push(
        `
        UPDATE ${ItemModelSql.storeItemTable} 
        SET ${ItemModelSql.qty} = GREATEST(${ItemModelSql.qty} + ${element.qty}, ${element.qty}) 
        WHERE ${ItemModelSql.stockOriginalItemId} = "${element.originalId}" AND ${ItemModelSql.branchId} = ${branchId}
        `
      );
      
    } else {
      values.push(`
        (
          "${element.stockItemId}",
          "${element.originalId}",
          "${element.qty}",
          "${adminId}",
          "${branchId}",
          "${createdAt}",
          "${element.location}",
          "${element.status}"
        )
    `);
    }
    
  }

  console.log(...values);

  let sql =  []
  
  if(values && values.length > 0)  {
    sql.push(`
    INSERT INTO ${storeItem} 
      (
        ${ItemModelSql.stockItemId},
        ${ItemModelSql.stockOriginalItemId},
        ${ItemModelSql.qty},
        ${ItemModelSql.adminId},
        ${ItemModelSql.branchId},
        ${ItemModelSql.createdAt},
        ${ItemModelSql.location},
        ${ItemModelSql.status}
      )
      VALUES ${values.join(',').toString()}`)
  }

    if(toBeUpdatedItems && toBeUpdatedItems.length > 0) {
      sql.push(...toBeUpdatedItems);
    }


console.log('sql', sql);
	
    stock_management.query(sql.join(','), (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, { });
	});
});

router.post('/addAllStockItemToDB', (req, res) => {
  let {
    // itemModelList,
    adminId,
    branchId,
    createdAt,} = req.body;
  const values = [];
  branchId = 3;
  adminId = 1;
  createdAt = '2021-03-13 5:02:01.647';
  
  
  const sql1 = `SELECT *from items where id > 545`;

  stock_management.query(sql1, (err, itemModelList) => {

    for (let index = 0; index < itemModelList.length; index++) {
      const element = itemModelList[index];
      values.push(`
      (
        "${element.id}",
        "${element.qty}",
        "${adminId}",
        "${branchId}",
        "${createdAt}",
        "${element.location}",
        "${element.status}"
      )
      `);
    }


  console.log(...values);

  const sql = `
  INSERT INTO ${storeItem} 
    (
      ${ItemModelSql.stockItemId},
      ${ItemModelSql.qty},
      ${ItemModelSql.adminId},
      ${ItemModelSql.branchId},
      ${ItemModelSql.createdAt},
      ${ItemModelSql.location},
      ${ItemModelSql.status}
    )
    VALUES ${values.join(',').toString()}`;
    console.log(sql);
	
    stock_management.query(sql, (err2, results) => {
      console.log(err2);
		if (err2) return Utils.ErrMsg(res, {msg: err2.code});
		return Utils.SuccessMsg(res, { });
	});
  });

  
});

router.post('/getAllItems', (req, res) => {
  let sql = `SELECT 
  b.name AS "${ItemModelSql.branchName}", b.type AS "${ItemModelSql.branchType}",
  s.${ItemModelSql.branchId}, s.${ItemModelSql.qty}, s.${ItemModelSql.adminId}, s.${ItemModelSql.createdAt}, s.${ItemModelSql.location}, s.${ItemModelSql.status}, s.${ItemModelSql.id}, s.${ItemModelSql.stockItemId},s.${ItemModelSql.stockOriginalItemId},
  i.${ItemModelSql.name}, i.${ItemModelSql.mark}, i.${ItemModelSql.groupp},i.${ItemModelSql.barcode}, i.${ItemModelSql.partNo},i.${ItemModelSql.originalId}, i.${ItemModelSql.brand}, i.${ItemModelSql.image}, i.${ItemModelSql.unitPrice}, i.${ItemModelSql.maxPrice}, i.${ItemModelSql.expireDate}, i.${ItemModelSql.minPrice}, i.${ItemModelSql.description}, i.${ItemModelSql.purchasePrice}, i.${ItemModelSql.manufacturer}, i.${ItemModelSql.manufacturerDate}, i.${ItemModelSql.measurement}
  FROM ${storeItem} s 
  INNER JOIN items i ON s.${ItemModelSql.stockItemId} = i.${ItemModelSql.id} 
  LEFT JOIN ${branch_model.branchTable} b ON s.${ItemModelSql.branchId} = b.${ItemModelSql.id}
  `;

  stock_management.query(sql, (err, results) => {
    console.log(err);
  if (err) return Utils.ErrMsg(res, {msg: err.code});
  return Utils.SuccessMsg(res, {data: results });
});
});

router.post('/getPaginatedItems', (req, res) => {
  let {page, sortBy, filterBy} = req.body;
  console.log(req.body);
  
  let sql = '';
  let where = '';

  // advance filter
  if(filterBy && filterBy.length > 0) {
    where = `WHERE ${filterBy.map((v) => `${(storeItemsCol.indexOf(v.technicalName) > -1) ? 's' : 'i'}.${v.technicalName} ${v.value}`).join(' AND ')}`;
  }

  sortBy = sortBy && sortBy.technicalName ? sortBy : {technicalName: 'createdAt', value: 'DESC'};

  const orderBy = `${(storeItemsCol.indexOf(sortBy.technicalName) > -1) ? 's' : 'i'}.${sortBy.technicalName}`

  const {limits, skip} = optimizeQueryPagination(page)

    sql = `SELECT 
    b.name AS "${ItemModelSql.branchName}", b.type AS "${ItemModelSql.branchType}",
    s.${ItemModelSql.branchId}, s.${ItemModelSql.qty}, s.${ItemModelSql.adminId}, s.${ItemModelSql.createdAt}, s.${ItemModelSql.location}, s.${ItemModelSql.status}, s.${ItemModelSql.id}, s.${ItemModelSql.stockItemId},s.${ItemModelSql.stockOriginalItemId},
    i.${ItemModelSql.name}, i.${ItemModelSql.mark}, i.${ItemModelSql.groupp},i.${ItemModelSql.barcode}, i.${ItemModelSql.partNo},i.${ItemModelSql.originalId}, i.${ItemModelSql.brand}, i.${ItemModelSql.image}, i.${ItemModelSql.unitPrice}, i.${ItemModelSql.maxPrice}, i.${ItemModelSql.expireDate}, i.${ItemModelSql.minPrice}, i.${ItemModelSql.description}, i.${ItemModelSql.purchasePrice}, i.${ItemModelSql.manufacturer}, i.${ItemModelSql.manufacturerDate}, i.${ItemModelSql.measurement}
    FROM ${storeItem} s 
    INNER JOIN items i ON s.${ItemModelSql.stockItemId} = i.${ItemModelSql.id} 
    LEFT JOIN ${branch_model.branchTable} b ON s.${ItemModelSql.branchId} = b.${ItemModelSql.id}
    ${where}
    ORDER BY ${orderBy} ${sortBy.value} LIMIT ${limits} OFFSET ${skip} `;

  stock_management.query(sql, (err, results) => {
    console.log(err);
  if (err) return Utils.ErrMsg(res, {msg: err.code});
  return Utils.SuccessMsg(res, {data: results });
});
});
router.post('/getPaginatedSearchItems', (req, res) => {
  const {q, page,customBranchId, filterBy} = req.body;
  console.log(req.body);
  const {limits, skip} = optimizeQueryPagination(page)
  // const sql = `SELECT * FROM ${storeItem} WHERE ${ItemModelSql.name} LIKE '%${q}%' LIMIT ${limits} OFFSET ${skip}`;
  console.log(customBranchId);
  const defaultWhere = `
  (i.${ItemModelSql.name} LIKE '%${q}%'
  OR
  i.${ItemModelSql.partNo} LIKE '%${q}%'
  OR
  i.${ItemModelSql.barcode} LIKE '%${q}%')
  `;

  let where = customBranchId && customBranchId !== 99999 ? `
  s.${ItemModelSql.branchId} = ${customBranchId} AND 
  ${defaultWhere}
  ` : defaultWhere;

  // advance filter
  if(filterBy && filterBy.length > 0) {
    where += ` AND ${filterBy.map((v) => `${(storeItemsCol.indexOf(v.technicalName) > -1) ? 's' : 'i'}.${v.technicalName} ${v.value}`).join(' AND ')}`;
  }

  const sql = `SELECT 
  b.name AS "${ItemModelSql.branchName}", b.type AS "${ItemModelSql.branchType}",
  s.${ItemModelSql.branchId}, s.${ItemModelSql.qty}, s.${ItemModelSql.adminId}, s.${ItemModelSql.createdAt}, s.${ItemModelSql.location}, s.${ItemModelSql.status}, s.${ItemModelSql.id}, s.${ItemModelSql.stockItemId},s.${ItemModelSql.stockOriginalItemId},
  i.${ItemModelSql.name}, i.${ItemModelSql.mark}, i.${ItemModelSql.groupp},i.${ItemModelSql.barcode}, i.${ItemModelSql.partNo},i.${ItemModelSql.originalId}, i.${ItemModelSql.brand}, i.${ItemModelSql.image}, i.${ItemModelSql.unitPrice}, i.${ItemModelSql.maxPrice}, i.${ItemModelSql.expireDate}, i.${ItemModelSql.minPrice}, i.${ItemModelSql.description}, i.${ItemModelSql.purchasePrice}, i.${ItemModelSql.manufacturer}, i.${ItemModelSql.manufacturerDate}, i.${ItemModelSql.measurement}
  FROM ${storeItem} s 
  INNER JOIN items i ON s.${ItemModelSql.stockItemId} = i.${ItemModelSql.id} 
  LEFT JOIN ${branch_model.branchTable} b ON s.${ItemModelSql.branchId} = b.${ItemModelSql.id}
  WHERE ${where}
  LIMIT ${limits} OFFSET ${skip} `;

    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});
router.post('/getItemIn', (req, res) => {
  const {ids} = req.body;
  console.log(ids);
  const sql = `SELECT 
  b.name AS "${ItemModelSql.branchName}", b.type AS "${ItemModelSql.branchType}",
  s.${ItemModelSql.branchId}, s.${ItemModelSql.qty}, s.${ItemModelSql.adminId}, s.${ItemModelSql.createdAt}, s.${ItemModelSql.location}, s.${ItemModelSql.status}, s.${ItemModelSql.id}, s.${ItemModelSql.stockItemId},s.${ItemModelSql.stockOriginalItemId},
  i.${ItemModelSql.name}, i.${ItemModelSql.mark}, i.${ItemModelSql.groupp},i.${ItemModelSql.barcode}, i.${ItemModelSql.partNo},i.${ItemModelSql.originalId}, i.${ItemModelSql.brand}, i.${ItemModelSql.image}, i.${ItemModelSql.unitPrice}, i.${ItemModelSql.maxPrice}, i.${ItemModelSql.expireDate}, i.${ItemModelSql.minPrice}, i.${ItemModelSql.description}, i.${ItemModelSql.purchasePrice}, i.${ItemModelSql.manufacturer}, i.${ItemModelSql.manufacturerDate}, i.${ItemModelSql.measurement}
  FROM ${storeItem} s 
  INNER JOIN items i ON s.${ItemModelSql.stockItemId} = i.${ItemModelSql.id} 
  LEFT JOIN ${branch_model.branchTable} b ON s.${ItemModelSql.branchId} = b.${ItemModelSql.id}
  WHERE s.${ItemModelSql.id} IN (${ids.join(', ')})`;
    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});

router.post('/updateItem', (req, res) => {
  const {id, key, value} = req.body;
	const sql = `UPDATE ${storeItem} SET ${key} = ${value} WHERE ${ItemModelSql.id} = ${id}`;
    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});

router.post('/deductItem', async (req, res) => {
  const {id, key, value} = req.body;
  deductStoreItem(id, key, value).then((results) => {
    return Utils.SuccessMsg(res, {data: results });
  }).catch((err) => Utils.ErrMsg(res, {msg: err.code}));

});

// router.post('/deductItem', (req, res) => {
//   const {id, key, value} = req.body;
// 	const sql = `UPDATE ${storeItem} SET ${key} = GREATEST(${key} - ${value}, 0) WHERE ${ItemModelSql.id} = ${id}`;
//     stock_management.query(sql, (err, results) => {
//       console.log(err);
// 		if (err) return Utils.ErrMsg(res, {msg: err.code});
// 		return Utils.SuccessMsg(res, {data: results });
// 	});
// });

router.post('/addQtyItem', (req, res) => {
  const {id, key, value} = req.body;
	const sql = `UPDATE ${storeItem} SET ${key} = GREATEST(${key} + ${value}, ${value}) WHERE ${ItemModelSql.id} = ${id}`;
    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});

router.post('/updateAllItem', (req, res) => {
  const {id, itemModel} = req.body;
  const sql = `UPDATE ${storeItem} SET 
  ${ItemModelSql.qty} = "${itemModel.qty}",
  ${ItemModelSql.branchId} = "${itemModel.branchId}",
  ${ItemModelSql.location} = "${itemModel.location}",
  ${ItemModelSql.status} = "${itemModel.status}" 
  WHERE ${ItemModelSql.id} = ${id}`;
    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});

router.post('/deleteItem', (req, res) => {
  const {id} = req.body;
	const sql = `DELETE FROM ${storeItem} WHERE ${ItemModelSql.id} = ${id}`;
    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});
router.post('/itemsCount', (req, res) => {
	const sql = `SELECT COUNT(*) FROM ${storeItem}`;
    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});
router.post('/getLowInStoreCount', (req, res) => {
  const {lowInStock = 0} = req.body;
	const sql = `SELECT COUNT(*) AS stockCount FROM ${storeItem} WHERE ${ItemModelSql.qty} <= ${lowInStock}`;
    stock_management.query(sql, (err, results) => {
      console.log(err);
    if (err) return Utils.ErrMsg(res, {msg: err.code});
    console.log(results);
		return Utils.SuccessMsg(res, {data: results[0]['stockCount'] });
	});
});

router.post('/getPaginatedLowInStoreItems', (req, res) => {
  const {page, lowInStock = 0} = req.body;
  const {limits, skip} = optimizeQueryPagination(page)
	const sql = `SELECT 
  b.name AS "${ItemModelSql.branchName}", b.type AS "${ItemModelSql.branchType}",
  s.${ItemModelSql.branchId}, s.${ItemModelSql.qty}, s.${ItemModelSql.adminId}, s.${ItemModelSql.createdAt}, s.${ItemModelSql.location}, s.${ItemModelSql.status}, s.${ItemModelSql.id}, s.${ItemModelSql.stockItemId},s.${ItemModelSql.stockOriginalItemId},
  i.${ItemModelSql.name}, i.${ItemModelSql.mark}, i.${ItemModelSql.groupp},i.${ItemModelSql.barcode}, i.${ItemModelSql.partNo},i.${ItemModelSql.originalId}, i.${ItemModelSql.brand}, i.${ItemModelSql.image}, i.${ItemModelSql.unitPrice}, i.${ItemModelSql.maxPrice}, i.${ItemModelSql.expireDate}, i.${ItemModelSql.minPrice}, i.${ItemModelSql.description}, i.${ItemModelSql.purchasePrice}, i.${ItemModelSql.manufacturer}, i.${ItemModelSql.manufacturerDate}, i.${ItemModelSql.measurement}
  FROM ${storeItem} s 
  INNER JOIN items i ON s.${ItemModelSql.stockItemId} = i.${ItemModelSql.id} 
  LEFT JOIN ${branch_model.branchTable} b ON s.${ItemModelSql.branchId} = b.${ItemModelSql.id}
  WHERE s.${ItemModelSql.qty} <= ${lowInStock} ORDER BY s.${ItemModelSql.createdAt} DESC LIMIT ${limits} OFFSET ${skip}`;
    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});

router.post('/transferItemFromStoreBranchToStoreBranch', (req, res) => {
  const {branchId, items  } = req.body;
  console.log(branchId, items);
  /**
   * @items
   *   {
      id: 1,
      qty: 2,
      all: false // if all is true ignore qty
    }
  */
    
 let sql = [];
 let lengths = [];
  if(items && items.length){
    for (let index = 0; index < items.length; index++) {
      const element = items[index];
      if(element.all === true){
        sql.push(`UPDATE ${storeItem} SET ${ItemModelSql.branchId} = ${branchId} WHERE ${ItemModelSql.id} = ${element.id}`);
        lengths.push(index)
      } else {
        // sql.push(`UPDATE FROM ${storeItem} WHERE ${ItemModelSql.id} = ${element.id} SET ${ItemModelSql.branchId} = ${branchId}`);
      }
      
    }
  }
    stock_management.query(sql.join(';'),lengths, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});


router.post('/transferItemFromStoreToStock', (req, res) => {
  let {item, branchId, qty,adminId} = req.body;
    console.log(req.body);
    // 1. check if the item is exists in storeItemTable by stockItemId == id AND same branchId add the qty else create new item with the given itemID, branch ..
    // 2.
    const  isItemExistInStoreSql =
        `SELECT * FROM ${ItemModelSql.itemTable} 
        WHERE ${ItemModelSql.originalId} = "${item[ItemModelSql.originalId]}" AND ${ItemModelSql.branchId} = ${branchId} `;

    stock_management.query(isItemExistInStoreSql, (err1, results1) => {
      console.log(err1);
    if (err1) return Utils.ErrMsg(res, {msg: err1.code});
    console.log('results1', results1);
    if(results1 && results1.length > 0) {
      // the item is exist
      const sql = ` 
      UPDATE ${ItemModelSql.itemTable} 
      SET ${ItemModelSql.qty} = GREATEST(${ItemModelSql.qty} + ${qty}, ${qty}) 
      WHERE ${ItemModelSql.originalId} = "${item[ItemModelSql.originalId]}"
      AND ${ItemModelSql.branchId} = ${branchId}
      `;
      stock_management.query(sql, (err3, results3) => {
        if (err3) return Utils.ErrMsg(res, {msg: err3.code});
        const sql2 = `UPDATE ${storeItem} SET ${ItemModelSql.qty} = GREATEST(${ItemModelSql.qty} - ${qty}, 0) 
        WHERE ${ItemModelSql.id} = ${item.id}`;
        stock_management.query(sql2, (err4, results4) => {
          if (err4) return Utils.ErrMsg(res, {msg: err4.code});
          return Utils.SuccessMsg(res, { });
        })
      })

      
    } else {
      item.createdAt = new Date(Date.now()).toString();
      item.qty = qty;
      item.location = '';
      item.branchId = branchId;
      item.adminId = adminId;
      item.status = '0';
      // create new
      const sqll = `INSERT INTO ${ItemModelSql.itemTable} 
      (
        ${ItemModelSql.name},
        ${ItemModelSql.originalId},
        ${ItemModelSql.image},
        ${ItemModelSql.unitPrice},
        ${ItemModelSql.maxPrice},
        ${ItemModelSql.minPrice},
        ${ItemModelSql.qty},
        ${ItemModelSql.adminId},
        ${ItemModelSql.partNo},
        ${ItemModelSql.barcode},
        ${ItemModelSql.branchId},
        ${ItemModelSql.brand},
        ${ItemModelSql.createdAt},
        ${ItemModelSql.description},
        ${ItemModelSql.groupp},
        ${ItemModelSql.location},
        ${ItemModelSql.manufacturer},
        ${ItemModelSql.manufacturerDate},
        ${ItemModelSql.mark},
        ${ItemModelSql.measurement},
        ${ItemModelSql.status},
        ${ItemModelSql.expireDate},
        ${ItemModelSql.expireRemindId},
        ${ItemModelSql.purchasePrice}
      )
      VALUES 
      (
        "${item.name}",
        "${item[ItemModelSql.originalId]}",
        "${item.image}",
        "${item.unitPrice || 0}",
        "${item.maxPrice}",
        "${item.minPrice}",
        "${item.qty}",
        "${item.adminId}",
        "${item.partNo}",
        "${item.barcode}",
        "${item.branchId}",
        "${item.brand}",
        "${item.createdAt}",
        "${item.description}",
        "${item.groupp}",
        "${item.location}",
        "${item.manufacturer}",
        "${item.manufacturerDate}",
        "${item.mark}",
        "${item.measurement}",
        "${item.status}",
        "${item.expireDate || ''}",
        "${item.expireRemindId || 9999}",
        "${item.purchasePrice}"
      )
      `;
      stock_management.query(sqll, (err, results) => {
          console.log(err);
        if (err) return Utils.ErrMsg(res, {msg: err.code});
        const sql2 = `UPDATE ${storeItem} SET ${ItemModelSql.qty} = GREATEST(${ItemModelSql.qty} - ${qty}, 0) WHERE ${ItemModelSql.id} = ${item.id}`;
        stock_management.query(sql2, (err3, results3) => {
          if (err3) return Utils.ErrMsg(res, {msg: err3.code});
          return Utils.SuccessMsg(res, { });
        })
      });
    }
	});
});

module.exports = router;