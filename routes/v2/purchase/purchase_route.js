/* @api /api/purchase */
const express = require('express');

const router = express.Router();
const { catchErrors } = require('../../../middleware/index');
const { isAuthenticated } = require('../../../middleware/isAuthenticated');
const {stock_management} = require('../../../core/db/mysql');
const Utils = require('../../../core/processor')
const {_PurchaseModelSql, _PurchaseType, _CreditStatus } = require('../../../models/purchase_model');
const UserModelSql = require('../../../models/user_model');
const BranchModelSql = require('../../../models/branch_model');
const { optimizeQueryPagination } = require('../../../Global/routeHelpers');
const { isDef, is } = require('../../../utils/helpers');
const item_model = require('../../../models/item_model');
const { deductStoreItem,deductStockItem } = require('../../v1/item_helper');
const purchaseTable = 'purchase'

router.post('/createPurchaseTable', (req, res) => {
	const sql = `CREATE TABLE ${purchaseTable}
  (
      ${_PurchaseModelSql.id} INTEGER AUTO_INCREMENT PRIMARY KEY,
      ${_PurchaseModelSql.items} JSON,
      ${_PurchaseModelSql.customerId} INTEGER,
      ${_PurchaseModelSql.adminId} INTEGER,
      ${_PurchaseModelSql.branchId} INTEGER,
      ${_PurchaseModelSql.totalAmount} REAL(10,2),
      ${_PurchaseModelSql.unpaidAmount} REAL(10,2),
      ${_PurchaseModelSql.stockTotal} REAL(10,2),
      ${_PurchaseModelSql.storeTotal} REAL(10,2),
      ${_PurchaseModelSql.customerName} TEXT,
      ${_PurchaseModelSql.customerPhoneNo} TEXT,
      ${_PurchaseModelSql.createdAt} DATE,
      ${_PurchaseModelSql.updatedAt} TEXT,
      ${_PurchaseModelSql.type} TEXT,
      ${_PurchaseModelSql.returnDate} TEXT,
      ${_PurchaseModelSql.creditStatus} TEXT,
      ${_PurchaseModelSql.creditPaymentAmountHistory} JSON
  )`;
 
  stock_management.query(sql, (err, results) => {
    console.log(err);
    if (err) return Utils.ErrMsg(res, {msg: err.code});
    return Utils.SuccessMsg(res, { });
  });

		
});


router.post('/addNewPurchaseToDB', (req, res) => {
  const {purchaseModelData} = req.body;
  console.log(purchaseModelData);
  const item = JSON.stringify(purchaseModelData.items)
  const creditPaymentAmountHistory = JSON.stringify(purchaseModelData.creditPaymentAmountHistory)
  if(purchaseModelData.createdAt) {
    purchaseModelData.createdAt = purchaseModelData.createdAt.slice(0,10)
  }
  const sql = `INSERT INTO ${purchaseTable}
  (
    ${_PurchaseModelSql.items},
    ${_PurchaseModelSql.customerId},
    ${_PurchaseModelSql.adminId},
    ${_PurchaseModelSql.customerName},
    ${_PurchaseModelSql.customerPhoneNo},
    ${_PurchaseModelSql.createdAt},
    ${_PurchaseModelSql.updatedAt},
    ${_PurchaseModelSql.type},
    ${_PurchaseModelSql.returnDate},
    ${_PurchaseModelSql.creditStatus},
    ${_PurchaseModelSql.totalAmount},
    ${_PurchaseModelSql.stockTotal},
    ${_PurchaseModelSql.storeTotal},
    ${_PurchaseModelSql.unpaidAmount},
    ${_PurchaseModelSql.branchId},
    ${_PurchaseModelSql.creditPaymentAmountHistory}
  
  )
  VALUES
  (
    ${item},
    "${purchaseModelData.customerId || 9999}",
    "${purchaseModelData.adminId|| 9999}",
    "${purchaseModelData.customerName  || 'N/A'}",
    "${purchaseModelData.customerPhoneNo || 'N/A'}",
    "${purchaseModelData.createdAt}",
    "${purchaseModelData.updatedAt}",
    "${purchaseModelData.type}",
    "${purchaseModelData.returnDate}",
    "${purchaseModelData.creditStatus}",
    "${purchaseModelData.totalAmount || 0}",
    "${purchaseModelData.stockTotal || 0}",
    "${purchaseModelData.storeTotal || 0}",
    "${purchaseModelData.unpaidAmount || 0}",
    "${purchaseModelData.branchId}",
    ${creditPaymentAmountHistory}
  )`
  stock_management.query(sql, async (err, results) => {
    console.log(err);
    if (err) return Utils.ErrMsg(res, {msg: err.code});
    await removeQtyFromStock(purchaseModelData.items);
		return Utils.SuccessMsg(res, { });
  });
});

const removeQtyFromStock = async (items) => {
  let promises = [];
  console.log(JSON.parse(items));
  for (let index = 0; index < JSON.parse(items).items.length; index++) {
    const element = JSON.parse(items).items[index];
    if (isDef(element.itemId) && is(element.qty, Number)) {
      if(isDef(element.storeId)) {
        promises.push(deductStoreItem(element.storeId, item_model.qty, element.qty));
      } else {
        promises.push(deductStockItem(element.itemId, item_model.qty, element.qty));
      }
    }  
  }
  
  try {
    console.log(`promises ${promises.length}`);
    await Promise.all(promises);
  } catch (error) {
    console.log(error);
  }
}

router.post('/updateCreditPurchase', (req, res) => {
  const {id, history, status, unpaid} = req.body;
  const historyy = JSON.stringify(history)

  const sql = `UPDATE ${purchaseTable} SET 
  ${_PurchaseModelSql.creditPaymentAmountHistory} = ${historyy}, 
  ${_PurchaseModelSql.creditStatus} = ${status},  
  ${_PurchaseModelSql.unpaidAmount} = ${unpaid || 0}  
  WHERE ${_PurchaseModelSql.id} = ${id}`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, { });
  });

});

router.post('/getPaginatedPurchase', (req, res) => {
  let {page, filterBy, sortBy} = req.body;

  console.log(page, sortBy, filterBy);

  let sql = '';
  sortBy = sortBy && sortBy.technicalName ? sortBy : {technicalName: 'createdAt', value: 'DESC'};


  const {limits, skip} = optimizeQueryPagination(page)

  if (filterBy && filterBy.technicalName && filterBy.value) {

    sql = `SELECT p.*,
    b.name AS "${_PurchaseModelSql.branchName}", b.type AS "${_PurchaseModelSql.branchType}",
    u.${UserModelSql.username} AS "${_PurchaseModelSql.adminName}", u.${UserModelSql.role} AS "${_PurchaseModelSql.adminRole}"
    FROM ${purchaseTable} p  
    LEFT JOIN ${BranchModelSql.branchTable} b ON p.${_PurchaseModelSql.branchId} = b.${BranchModelSql.id}
    LEFT JOIN ${UserModelSql.userTable} u ON p.${_PurchaseModelSql.adminId} = u.${UserModelSql.id}
    WHERE p.${filterBy.technicalName} = "${filterBy.value}"
    ORDER BY p.${sortBy.technicalName} ${sortBy.value} 
    LIMIT ${limits} OFFSET ${skip}`;
         

  } else {

      // sql = `
      // SELECT p.*, pi.amount, pi.storeId FROM ${purchaseTable} p 
      // LEFT JOIN purchase_items pi ON p.${_PurchaseModelSql.id} = pi.${_PurchaseModelSql.purchaseId}  
      // ORDER BY p.${sortBy.technicalName} ${sortBy.value} LIMIT ${limits} OFFSET ${skip}`;

      // sql = `SELECT * FROM ${purchaseTable} 
      // ORDER BY ${sortBy.technicalName} ${sortBy.value} LIMIT ${limits} OFFSET ${skip}`;

      sql = `SELECT p.*,
    b.name AS "${_PurchaseModelSql.branchName}", b.type AS "${_PurchaseModelSql.branchType}",
    u.${UserModelSql.username} AS "${_PurchaseModelSql.adminName}", u.${UserModelSql.role} AS "${_PurchaseModelSql.adminRole}"
    FROM ${purchaseTable} p  
    LEFT JOIN ${BranchModelSql.branchTable} b ON p.${_PurchaseModelSql.branchId} = b.${BranchModelSql.id}
    LEFT JOIN ${UserModelSql.userTable} u ON p.${_PurchaseModelSql.adminId} = u.${UserModelSql.id}
    ORDER BY p.${sortBy.technicalName} ${sortBy.value} 
    LIMIT ${limits} OFFSET ${skip}`;

      // sql = `
      // SELECT * FROM ${purchaseTable} WHERE items->'$.items[*].branchType' = '1'
      // `;
      // sql = `
      // SELECT (JSON_EXTRACT(items, '$.items[*].amount')) AS pricces FROM ${purchaseTable}
      // `;
      
  }
  stock_management.query(sql, (err, results) => {
    console.log(err);
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/getSearchPaginatedPurchase', (req, res) => {
  const {page,q} = req.body;
  const {limits, skip} = optimizeQueryPagination(page)
  // const sql = `SELECT * FROM ${purchaseTable} WHERE ${_PurchaseModelSql.customerName} LIKE '%${q}%' OR ${_PurchaseModelSql.customerPhoneNo} LIKE '%$q%' LIMIT ${limits} OFFSET ${skip}`;

  sql = `SELECT p.*,
  b.name AS "${_PurchaseModelSql.branchName}", b.type AS "${_PurchaseModelSql.branchType}",
  u.${UserModelSql.username} AS "${_PurchaseModelSql.adminName}", u.${UserModelSql.role} AS "${_PurchaseModelSql.adminRole}"
  FROM ${purchaseTable} p  
  LEFT JOIN ${BranchModelSql.branchTable} b ON p.${_PurchaseModelSql.branchId} = b.${BranchModelSql.id}
  LEFT JOIN ${UserModelSql.userTable} u ON p.${_PurchaseModelSql.adminId} = u.${UserModelSql.id}
  WHERE p.${_PurchaseModelSql.customerName} LIKE '%${q}%' OR p.${_PurchaseModelSql.customerPhoneNo} LIKE '%${q}%'
  ORDER BY p.${sortBy.technicalName} ${sortBy.value} 
  LIMIT ${limits} OFFSET ${skip}`;

  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/getAllPurchaseByDate', (req, res) => {
  const {startDateTime, endDateTime,chooseReportDateType} = req.body;
  console.log(startDateTime,endDateTime,chooseReportDateType);
  if(chooseReportDateType == 'ChooseReportDateType.thisWeek'){
    sql = `SELECT p.*,
    u.${UserModelSql.username} AS "${_PurchaseModelSql.adminName}", u.${UserModelSql.role} AS "${_PurchaseModelSql.adminRole}"
    FROM ${purchaseTable} p  
    LEFT JOIN ${UserModelSql.userTable} u ON p.${_PurchaseModelSql.adminId} = u.${UserModelSql.id}
    WHERE ${_PurchaseModelSql.createdAt} >= DATE(NOW()) - INTERVAL 7 DAY`
  } else if(chooseReportDateType == 'ChooseReportDateType.thisMonth'){ 
    sql = `SELECT p.*,
    u.${UserModelSql.username} AS "${_PurchaseModelSql.adminName}", u.${UserModelSql.role} AS "${_PurchaseModelSql.adminRole}"
    FROM ${purchaseTable} p  
    LEFT JOIN ${UserModelSql.userTable} u ON p.${_PurchaseModelSql.adminId} = u.${UserModelSql.id}
    WHERE MONTH(${_PurchaseModelSql.createdAt}) = MONTH(CURRENT_DATE()) AND YEAR(${_PurchaseModelSql.createdAt}) = YEAR(CURRENT_DATE())` 
  } else if(chooseReportDateType == 'ChooseReportDateType.thisYear'){ 
    sql = `SELECT p.*,
    u.${UserModelSql.username} AS "${_PurchaseModelSql.adminName}", u.${UserModelSql.role} AS "${_PurchaseModelSql.adminRole}"
    FROM ${purchaseTable} p  
    LEFT JOIN ${UserModelSql.userTable} u ON p.${_PurchaseModelSql.adminId} = u.${UserModelSql.id}
    WHERE YEAR(${_PurchaseModelSql.createdAt}) = YEAR(CURDATE())` 
  } else {
    sql = `SELECT p.*,
    u.${UserModelSql.username} AS "${_PurchaseModelSql.adminName}", u.${UserModelSql.role} AS "${_PurchaseModelSql.adminRole}"
    FROM ${purchaseTable} p  
    LEFT JOIN ${UserModelSql.userTable} u ON p.${_PurchaseModelSql.adminId} = u.${UserModelSql.id}
    WHERE ${_PurchaseModelSql.createdAt} BETWEEN "${startDateTime}" AND "${endDateTime}"`;
  }

    console.log(sql);
  stock_management.query(sql, (err, results) => {
    console.log(err);
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/deletePurchaseFromDB', (req, res) => {
  const {id} = req.body;
  const sql = `DELETE FROM ${purchaseTable} WHERE ${_PurchaseModelSql.id} = ${id}`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

module.exports = router;