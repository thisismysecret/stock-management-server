
/* @api /api/user */
const express = require('express');

const router = express.Router();
const { catchErrors } = require('../../../middleware/index');
const { isAuthenticated } = require('../../../middleware/isAuthenticated');
const {stock_management} = require('../../../core/db/mysql');
const Utils = require('../../../core/processor')
const MyCreditModelSql = require('../../../models/my_credit_model');
const { optimizeQueryPagination } = require('../../../Global/routeHelpers');

const  myCreditTable = 'mycredits';


router.post('/createMyCreditTable', (req, res) => {
	const sql = `CREATE TABLE ${myCreditTable}
  (
      ${MyCreditModelSql.id} INTEGER AUTO_INCREMENT PRIMARY KEY,
      ${MyCreditModelSql.items} JSON,
      ${MyCreditModelSql.adminId} INTEGER,
      ${MyCreditModelSql.companyIdentifer} TEXT,
      ${MyCreditModelSql.createdAt} TEXT,
      ${MyCreditModelSql.updatedAt} TEXT,
      ${MyCreditModelSql.creditStatus} TEXT,
      ${MyCreditModelSql.creditPaymentAmountHistory} JSON
  )`;
 
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
    return Utils.SuccessMsg(res, { });
  });

		
});

router.post('/updateMyCredit', (req, res) => {
  const {id, history, status} = req.body;
  const historyy = JSON.stringify(history)

  const sql = `UPDATE ${myCreditTable} SET 
  ${MyCreditModelSql.creditPaymentAmountHistory} = ${historyy}, 
  ${MyCreditModelSql.creditStatus} = ${status}
  WHERE ${MyCreditModelSql.id} = ${id}`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, { });
  });

});


router.post('/addNewMyCreditToDB', (req, res) => {
  const {myCreditModelData} = req.body;
  console.log(req.body);
  const items = JSON.stringify(myCreditModelData.items);

  const sql = `INSERT INTO ${myCreditTable}
  (
    ${MyCreditModelSql.items},
    ${MyCreditModelSql.adminId},
    ${MyCreditModelSql.companyIdentifer},
    ${MyCreditModelSql.createdAt},
    ${MyCreditModelSql.updatedAt},
    ${MyCreditModelSql.creditStatus},
    ${MyCreditModelSql.creditPaymentAmountHistory}
  
  )
  VALUES
  (
    ${items},
    "${myCreditModelData.adminId}",
    "${myCreditModelData.companyIdentifer}",
    "${myCreditModelData.createdAt}",
    "${myCreditModelData.updatedAt}",
    "${myCreditModelData.creditStatus}",
    ${creditPaymentAmountHistory}
  )`
  stock_management.query(sql, (err, results) => {
    console.log(err);
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });
});

router.post('/getPaginatedMyCredit', (req, res) => {
  const {page} = req.body;
  const {limits, skip} = optimizeQueryPagination(page)
  const sql = `SELECT * FROM ${myCreditTable} 
  ORDER BY ${MyCreditModelSql.createdAt} DESC
  LIMIT ${limits} OFFSET ${skip} `;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/getSearchPaginatedMyCredit', (req, res) => {
  const {page, q} = req.body;
  const {limits, skip} = optimizeQueryPagination(page)
  const sql = `SELECT * FROM ${myCreditTable} WHERE ${MyCreditModelSql.companyIdentifer} LIKE '%${q}%' LIMIT ${limits} OFFSET ${skip}`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/updateSingle', (req, res) => {
  const {id, key, value} = req.body;
  const sql = `UPDATE ${myCreditTable} SET ${key} = ${value} WHERE ${MyCreditModelSql.id} = ${id}`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});
router.post('/deleteMyCreditFromDB', (req, res) => {
  const {id} = req.body;
  const sql = `DELETE FROM ${myCreditTable} WHERE ${MyCreditModelSql.id} = ${id}`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});


module.exports = router;