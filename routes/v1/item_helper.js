const { stock_management } = require("../../core/db/mysql");
const item_model = require("../../models/item_model");

exports.deductStockItem = (id, key, value) => {
  return new Promise(function(resolve, reject) {
    const sql = `UPDATE ${item_model.itemTable} SET ${key} = GREATEST(${key} - ${value}, 0) WHERE ${item_model.id} = ${id}`;
    console.log(sql);
    stock_management.query(sql, (err, results) => {
      if(err) console.log(err);
      if (err) return reject(err);
      return resolve(results)
    });
  })
}

exports.deductStoreItem = (id, key, value) => {
  return new Promise(function(resolve, reject) {
    const sql = `UPDATE ${item_model.storeItemTable} SET ${key} = GREATEST(${key} - ${value}, 0) WHERE ${item_model.id} = ${id}`;
    stock_management.query(sql, (err, results) => {
      if(err) console.log(err);
      if (err) return reject(err);
      return resolve(results)
    });
  })
}