
/* @api /api/items */
const express = require('express');

const router = express.Router();
const { catchErrors } = require('../../../middleware/index');
const { isAuthenticated } = require('../../../middleware/isAuthenticated');
const {stock_management} = require('../../../core/db/mysql');
const Utils = require('../../../core/processor')
const UserModelSql = require('../../../models/user_model');
const { optimizeQueryPagination } = require('../../../Global/routeHelpers');

const  userTable = 'user';


router.post('/createUserTable', (req, res) => {
	const sql = `CREATE TABLE ${userTable}
  (
      ${UserModelSql.id} INTEGER AUTO_INCREMENT PRIMARY KEY,
      ${UserModelSql.username} TEXT,
      ${UserModelSql.avatar} TEXT,
      ${UserModelSql.role} TEXT,
      ${UserModelSql.password} TEXT,
      ${UserModelSql.salesBranchId} INTEGER,
      ${UserModelSql.phone} TEXT
  )`;
 
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
    return Utils.SuccessMsg(res, { });
  });

		
});


router.post('/addNewUserToDB', (req, res) => {
  const {userInfo} = req.body;
  const sql = `INSERT INTO ${userTable}
  (
    ${UserModelSql.username},
    ${UserModelSql.avatar},
    ${UserModelSql.role},
    ${UserModelSql.phone},
    ${UserModelSql.salesBranchId},
    ${UserModelSql.password}
  
  )
  VALUES
  (
    "${userInfo.username}",
    "${userInfo.avatar}",
    "${userInfo.role}",
    "${userInfo.phone}",
    "${userInfo.salesBranchId}",
    "${userInfo.password}"
  )`
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });
});


router.post('/isUserExist', (req, res) => {
  const {userInfo} = req.body;
  const sql = `SELECT * FROM ${userTable} WHERE ${UserModelSql.username} = "${userInfo.username}"`;
  stock_management.query(sql, (err, results) => {
    if (err) {
      console.log(err);
      return Utils.ErrMsg(res, {msg: err.code})
    };
    console.log(results && results.length > 0  ? true : false);
    isUserExits = results && results.length > 0  ? true : false;
		return Utils.SuccessMsg(res, {data: isUserExits });
  });

});

router.post('/isPasswordCorrect', (req, res) => {
  const {userInfo} = req.body;
  const sql = `SELECT * FROM ${userTable} WHERE ${UserModelSql.username} = "${userInfo.username}" AND ${UserModelSql.password} = "${userInfo.password}" `;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
    console.log(results && results.length > 0  ? true : false);
    isPasswordCo = results && results.length > 0  ? true : false;
		return Utils.SuccessMsg(res, {data: isPasswordCo });
  });

});

router.post('/getUserInfo', (req, res) => {
  const {userInfo} = req.body;
  const sql = `SELECT * FROM ${userTable} WHERE ${UserModelSql.username} = "${userInfo.username}" LIMIT 1`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/deleteUser', (req, res) => {
  const {id} = req.body;
  const sql = `DELETE FROM ${userTable} WHERE ${UserModelSql.id} = ${id}`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/updateUser', (req, res) => {
  const {id,userInfo} = req.body;
  const sql = `UPDATE ${userTable} SET 
  ${UserModelSql.avatar} = "${userInfo.avatar}",
  ${UserModelSql.username} = "${userInfo.username}",
  ${UserModelSql.phone} = "${userInfo.phone}",
  ${UserModelSql.role} = "${userInfo.role}",
  ${UserModelSql.salesBranchId} = "${userInfo.salesBranchId}",
  ${UserModelSql.password} = "${userInfo.password}"
  WHERE ${UserModelSql.id} = ${id}`;
  stock_management.query(sql, (err, results) => {
    console.log(err);
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});


router.post('/getPaginatedUsersInfo', (req, res) => {
  const {page} = req.body;
  const {limits, skip} = optimizeQueryPagination(page)
  const sql = `SELECT * FROM ${userTable} ORDER BY ${UserModelSql.id} DESC LIMIT ${limits} OFFSET ${skip}`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/getAllUsersInfo', (req, res) => {
  const sql = `SELECT * FROM ${userTable}`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});


module.exports = router;