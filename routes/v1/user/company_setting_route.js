/* @api /api/user */
const express = require('express');

const router = express.Router();
const { catchErrors } = require('../../../middleware/index');
const { isAuthenticated } = require('../../../middleware/isAuthenticated');
const {stock_management} = require('../../../core/db/mysql');
const Utils = require('../../../core/processor')
const CompanySettingModelSql = require('../../../models/company_setting_model');
const { optimizeQueryPagination } = require('../../../Global/routeHelpers');

const  companySettingTable = 'companysetting';


router.post('/createCompanySettingTable', (req, res) => {
	const sql = `CREATE TABLE ${companySettingTable}
  (
      ${CompanySettingModelSql.id} INTEGER AUTO_INCREMENT PRIMARY KEY,
      ${CompanySettingModelSql.name} TEXT,
      ${CompanySettingModelSql.logo} TEXT,
      ${CompanySettingModelSql.address} TEXT,
      ${CompanySettingModelSql.isOnline} INTEGER NOT NULL DEFAULT 1,
      ${CompanySettingModelSql.companyPassword} TEXT,
      ${CompanySettingModelSql.companyDomain} TEXT,
      ${CompanySettingModelSql.serverUrl} TEXT
  )`;
 
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
    return Utils.SuccessMsg(res, { });
  });

		
});


router.post('/initCompanySetting', (req, res) => {
  const sql = `INSERT INTO ${companySettingTable}
  (
    ${CompanySettingModelSql.name},
    ${CompanySettingModelSql.logo},
    ${CompanySettingModelSql.address},
    ${CompanySettingModelSql.isOnline},
    ${CompanySettingModelSql.companyDomain},
    ${CompanySettingModelSql.companyPassword},
    ${CompanySettingModelSql.serverUrl}
  )
  VALUES
  (
    "",
    "",
    "",
    0,
    "",
    "",
    ""
  )`
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });
});


router.post('/isCompanyDomainExist', (req, res) => {
  const {companySettingModelData} = req.body;
  const sql = `SELECT * FROM ${companySettingTable} WHERE ${CompanySettingModelSql.companyDomain} = "${companySettingModelData.companyDomain}"`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
    console.log(results && results.length > 0  ? true : false);
    isUserExits = results && results.length > 0  ? true : false;
		return Utils.SuccessMsg(res, {data: isUserExits });
  });

});

router.post('/isCompanyPasswordCorrect', (req, res) => {
  const {companySettingModelData} = req.body;
  const sql = `SELECT * FROM ${companySettingTable} WHERE ${CompanySettingModelSql.companyDomain} = "${companySettingModelData.companyDomain}" AND ${CompanySettingModelSql.companyPassword} = "${companySettingModelData.companyPassword}" `;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
    console.log(results && results.length > 0  ? true : false);
    isPasswordCo = results && results.length > 0  ? true : false;
		return Utils.SuccessMsg(res, {data: isPasswordCo });
  });

});

router.post('/getCompanySetting', (req, res) => {
  const sql = `SELECT * FROM ${companySettingTable} LIMIT 1`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
    console.log(results);
		return Utils.SuccessMsg(res, {data: results[0] });
  });

});

router.post('/updateCompanySetting', (req, res) => {
  const {id, companySettingModelData} = req.body;
  const sql = `UPDATE ${companySettingTable} SET 
  ${CompanySettingModelSql.name} = "${companySettingModelData.name}",
  ${CompanySettingModelSql.logo} = "${companySettingModelData.logo}",
  ${CompanySettingModelSql.address} = "${companySettingModelData.address}",
  ${CompanySettingModelSql.isOnline} = "${companySettingModelData.isOnline}"
  WHERE ${CompanySettingModelSql.id} = ${id}`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/updateSingleCompanySetting', (req, res) => {
  const {id, key, value} = req.body;
  const sql = `UPDATE ${companySettingTable} SET ${key} = "${value}" WHERE ${CompanySettingModelSql.id} = ${id} `;
  stock_management.query(sql, (err, results) => {
    console.log(err);
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});


module.exports = router;