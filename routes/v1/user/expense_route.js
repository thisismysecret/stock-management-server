/* @api /api/items */
const express = require('express');

const router = express.Router();
const {stock_management} = require('../../../core/db/mysql');
const Utils = require('../../../core/processor')
const ExpenseModelSql = require('../../../models/expense_model');
const { optimizeQueryPagination } = require('../../../Global/routeHelpers');

const expenseTable = ExpenseModelSql.expenseTable;

router.post('/createExpenseTable', (req, res) => {
	const sql_q = `CREATE TABLE ${expenseTable}
  (
    ${ExpenseModelSql.id} INTEGER AUTO_INCREMENT PRIMARY KEY ,
    ${ExpenseModelSql.date} TEXT,
    ${ExpenseModelSql.note} TEXT,
    ${ExpenseModelSql.amount} TEXT,
    ${ExpenseModelSql.payee} TEXT
  )`;
    stock_management.query(sql_q, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
    if (results && results.length === 0) return Utils.ErrMsg(res, { msg: 'Failed to create' });
		return Utils.SuccessMsg(res, { });
	});
});

router.post('/addExpense', (req, res) => {
  const {expenseModel} = req.body;
	const sql = `INSERT INTO ${expenseTable} 
  (
    ${ExpenseModelSql.date},
    ${ExpenseModelSql.note},
    ${ExpenseModelSql.amount},
    ${ExpenseModelSql.payee}
  )
  VALUES 
  (
    "${expenseModel.date}",
    "${expenseModel.note}",
    "${expenseModel.amount}",
    "${expenseModel.payee}"
  )
  `;
    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, { });
	});
});
router.post('/getPaginatedExpense', (req, res) => {
  let {page} = req.body;

  let sql = '';

  const {limits, skip} = optimizeQueryPagination(page)

  sql = `SELECT * FROM ${expenseTable} LIMIT ${limits} OFFSET ${skip}`;

  stock_management.query(sql, (err, results) => {
    console.log(err);
  if (err) return Utils.ErrMsg(res, {msg: err.code});
  return Utils.SuccessMsg(res, {data: results });
});
});
router.post('/getAllExpenses', (req, res) => {
  const sql = `SELECT * FROM ${expenseTable}`;

    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});

router.post('/getAllExpensesByDate', (req, res) => {
  const {startDateTime, endDateTime,chooseReportDateType} = req.body;
  console.log(startDateTime,endDateTime,chooseReportDateType);
  if(chooseReportDateType == 'ChooseReportDateType.thisWeek'){
    sql = `SELECT * FROM ${expenseTable} WHERE ${ExpenseModelSql.date} >= DATE(NOW()) - INTERVAL 7 DAY`
  } else if(chooseReportDateType == 'ChooseReportDateType.thisMonth'){ 
    sql = `SELECT * FROM ${expenseTable} WHERE MONTH(${ExpenseModelSql.date}) = MONTH(CURRENT_DATE()) AND YEAR(${ExpenseModelSql.date}) = YEAR(CURRENT_DATE())` 
  } else if(chooseReportDateType == 'ChooseReportDateType.thisYear'){ 
    sql = `SELECT * FROM ${expenseTable} WHERE YEAR(${ExpenseModelSql.date}) = YEAR(CURDATE())` 
  } else {
    sql = `SELECT * FROM ${expenseTable} WHERE ${ExpenseModelSql.date} BETWEEN "${startDateTime}" AND "${endDateTime}"`;
  }

    console.log(sql);
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/updateAllExpense', (req, res) => {
  const {id, expenseModel} = req.body;
  const sql = `UPDATE ${expenseTable} SET 
  ${ExpenseModelSql.date} = "${expenseModel.date}",
  ${ExpenseModelSql.payee} = "${expenseModel.payee}",
  ${ExpenseModelSql.amount} = "${expenseModel.amount}",
  ${ExpenseModelSql.note} = "${expenseModel.note}"
  WHERE ${ExpenseModelSql.id} = ${id}`;
    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});

router.post('/deleteExpense', (req, res) => {
  const {id} = req.body;
	const sql = `DELETE FROM ${expenseTable} WHERE ${ExpenseModelSql.id} = ${id}`;
    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});

module.exports = router;