/* @api /api/general */
const express = require("express");
const { stock_management } = require("../../../core/db/mysql");
const { itemTable } = require("../../../models/item_model");
const Utils = require("../../../core/processor");
const table_model = require("../../../models/table_model");

const router = express.Router();

router.post("/getDataByTableName", async (req, res) => {
  const {table} = req.body;
  stock_management.query(`SELECT * FROM ${table}`, (err, data) => {
    if (err) return Utils.ErrMsg(res, { msg: err.code });
    return Utils.SuccessMsg(res, { data })
  });
});

router.post("/getAllData", async (req, res) => {
  const sqls = [];
  sqls.push(`SELECT * FROM ${table_model.itemsTable}`);
  sqls.push(`SELECT * FROM ${table_model.storeItemsTable}`);
  sqls.push(`SELECT * FROM ${table_model.groupcollectionTable}`);
  sqls.push(`SELECT * FROM ${table_model.markTable}`);
  sqls.push(`SELECT * FROM ${table_model.brandTable}`);
  sqls.push(`SELECT * FROM ${table_model.measurementTable}`);
  sqls.push(`SELECT * FROM ${table_model.userTable}`);
  sqls.push(`SELECT * FROM ${table_model.purchaseTable}`);
  sqls.push(`SELECT * FROM ${table_model.historyTable}`);
  sqls.push(`SELECT * FROM ${table_model.companysettingTable}`);
  sqls.push(`SELECT * FROM ${table_model.customerTable}`);
  sqls.push(`SELECT * FROM ${table_model.billofladingTable}`);
  sqls.push(`SELECT * FROM ${table_model.branchTable}`);
  sqls.push(`SELECT * FROM ${table_model.expenseTable}`);
  sqls.push(`SELECT * FROM ${table_model.mycreditsTable}`);

  stock_management.query(sqls.join(";"), (err, results) => {
    if (err) return Utils.ErrMsg(res, { msg: err.code });
    return Utils.SuccessMsg(res, { data: {
      itemsTable: results[0],
      storeItemsTable: results[1],
      groupcollectionTable: results[2],
      markTable: results[3],
      brandTable: results[4],
      measurementTable: results[5],
      userTable: results[6],
      purchaseTable: results[7],
      historyTable: results[8],
      companysettingTable: results[9],
      customerTable: results[10],
      billofladingTable: results[11],
      branchTable: results[12],
      expenseTable: results[13],
      mycreditsTable: results[14],
    } });
  });
});

router.post("/deleteAllData", async (req, res) => {
  const { tables } = req.body;

  const tablesToDelete = []; // items, storeItems

  tables.forEach((element) => {
    tablesToDelete.push(`DELETE * FROM ${element}`);
  });

  if (tablesToDelete && tablesToDelete.length > 0) {
    stock_management.query(tablesToDelete.join(";"), (err, results) => {
      if (err) return Utils.ErrMsg(res, { msg: err.code });
      return Utils.SuccessMsg(res, { data: results });
    });
  }
  return Utils.SuccessMsg(res, { });
});

module.exports = router;
