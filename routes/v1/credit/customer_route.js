/* @api /api/customer */
const express = require('express');

const router = express.Router();
const { catchErrors } = require('../../../middleware/index');
const { isAuthenticated } = require('../../../middleware/isAuthenticated');
const {stock_management} = require('../../../core/db/mysql');
const Utils = require('../../../core/processor')
const CustomerModelSql = require('../../../models/customer_model');
const { optimizeQueryPagination } = require('../../../Global/routeHelpers');
const { _PurchaseModelSql } = require('../../../models/purchase_model');

const customerTable = 'customer';

router.post('/createCustomerTable', (req, res) => {
	const sql = `CREATE TABLE ${customerTable}
  (
      ${CustomerModelSql.id} INTEGER AUTO_INCREMENT PRIMARY KEY,
      ${CustomerModelSql.name} TEXT,
      ${CustomerModelSql.phone} TEXT,
      ${CustomerModelSql.company} TEXT,
      ${CustomerModelSql.address} TEXT
  )`;
 
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
    return Utils.SuccessMsg(res, { });
  });

		
});


router.post('/addNewCustomerToDB', (req, res) => {
  const {customerModel} = req.body;
  const sql = `INSERT INTO ${customerTable}
  (
    ${CustomerModelSql.name},
    ${CustomerModelSql.phone},
    ${CustomerModelSql.company},
    ${CustomerModelSql.address}

  )
  VALUES
  (
    "${customerModel.name}",
    "${customerModel.phone}",
    "${customerModel.company}",
    "${customerModel.address}"
  )`
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });
});




router.post('/getPaginatedCustomers', (req, res) => {
  const {page} = req.body;
  const {limits, skip} = optimizeQueryPagination(page)
  const sql = `SELECT * FROM ${customerTable} LIMIT ${limits} OFFSET ${skip}`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/deleteCustomerFromDB', (req, res) => {
  const {id} = req.body;
  const sql = `DELETE FROM ${customerTable} WHERE ${CustomerModelSql.id} = ${id}`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/updateCustomer', (req, res) => {
  const {customerModel} = req.body;
  const sql = `UPDATE ${customerTable} SET 
  ${CustomerModelSql.name} = "${customerModel.name}",
  ${CustomerModelSql.phone} = "${customerModel.phone}",
  ${CustomerModelSql.company} = "${customerModel.company}",
  ${CustomerModelSql.address} = "${customerModel.address}"  WHERE ${CustomerModelSql.id} = ${customerModel.id}`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/getCustomerWithThereCredit', (req, res) => {
  const {page} = req.body;

  const {limits, skip} = optimizeQueryPagination(page)

  const sql = `SELECT c.*, SUM(p.${_PurchaseModelSql.unpaidAmount}) AS unpaid 
  FROM ${_PurchaseModelSql.purchaseTable} p 
  INNER JOIN ${customerTable} c ON  p.${_PurchaseModelSql.customerId} = c.${_PurchaseModelSql.id} 
  WHERE p.${_PurchaseModelSql.unpaidAmount} > 0
  GROUP BY ${_PurchaseModelSql.customerId}
  LIMIT ${limits} OFFSET ${skip}
  `;
  stock_management.query(sql, (err, results) => {
    if (err) {
      console.log(err);
      return Utils.ErrMsg(res, {msg: err.code})
    };

    return Utils.SuccessMsg(res, { data: results });
  });
});

module.exports = router;