/* @api /api/purchase */
const express = require('express');

const router = express.Router();
const { catchErrors } = require('../../../middleware/index');
const { isAuthenticated } = require('../../../middleware/isAuthenticated');
const {stock_management} = require('../../../core/db/mysql');
const Utils = require('../../../core/processor')
const {_PurchaseModelSql,_PurchaseType, _CreditStatus } = require('../../../models/purchase_model');
const BranchModelSql = require('../../../models/branch_model');
const UserModelSql = require('../../../models/user_model');
const { optimizeQueryPagination } = require('../../../Global/routeHelpers');

const purchaseTable = 'purchase'


router.post('/getPaginatedCredits', (req, res) => {
  let {page, filterBy, sortBy} = req.body;

  console.log(page, sortBy, filterBy);

  let sql = '';
  sortBy = sortBy && sortBy.technicalName ? sortBy : {technicalName: 'createdAt', value: 'DESC'};
  let where = '';


  const {limits, skip} = optimizeQueryPagination(page)


  if (filterBy && filterBy.technicalName && filterBy.value) {

    where = `WHERE p.${_PurchaseModelSql.type} = "${_PurchaseType.credit}" AND p.${filterBy.technicalName} = "${filterBy.value}"`;

  } else {

    where = `WHERE p.${_PurchaseModelSql.type} = "${_PurchaseType.credit}" AND p.${_PurchaseModelSql.creditStatus} != ${_CreditStatus.payed}`;
  }

  sql = `SELECT p.*,
    b.name AS "${_PurchaseModelSql.branchName}", b.type AS "${_PurchaseModelSql.branchType}",
    u.${UserModelSql.username} AS "${_PurchaseModelSql.adminName}", u.${UserModelSql.role} AS "${_PurchaseModelSql.adminRole}"
    FROM ${purchaseTable} p  
    LEFT JOIN ${BranchModelSql.branchTable} b ON p.${_PurchaseModelSql.branchId} = b.${BranchModelSql.id}
    LEFT JOIN ${UserModelSql.userTable} u ON p.${_PurchaseModelSql.adminId} = u.${UserModelSql.id}
    ${where}
    ORDER BY p.${sortBy.technicalName} ${sortBy.value} 
    LIMIT ${limits} OFFSET ${skip}`;


  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/getPaginatedCreditsByCustomerId', (req, res) => {
  let {page, customerId} = req.body;

  console.log(req.body);

  let sortBy = {technicalName: 'createdAt', value: 'DESC'};

  const {limits, skip} = optimizeQueryPagination(page)

 let sql = `SELECT p.*,
    b.name AS "${_PurchaseModelSql.branchName}", b.type AS "${_PurchaseModelSql.branchType}",
    u.${UserModelSql.username} AS "${_PurchaseModelSql.adminName}", u.${UserModelSql.role} AS "${_PurchaseModelSql.adminRole}"
    FROM ${purchaseTable} p  
    LEFT JOIN ${BranchModelSql.branchTable} b ON p.${_PurchaseModelSql.branchId} = b.${BranchModelSql.id}
    LEFT JOIN ${UserModelSql.userTable} u ON p.${_PurchaseModelSql.adminId} = u.${UserModelSql.id}
    WHERE p.${_PurchaseModelSql.type} = "${_PurchaseType.credit}" AND p.${_PurchaseModelSql.customerId} = "${customerId}"
    ORDER BY p.${sortBy.technicalName} ${sortBy.value} 
    LIMIT ${limits} OFFSET ${skip}`;


  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/getSearchPaginatedCredits', (req, res) => {
  const {q, page} = req.body;
  const {limits, skip} = optimizeQueryPagination(page)
  // const sql = `SELECT * FROM ${purchaseTable} WHERE ${_PurchaseModelSql.type} = ${_PurchaseType.credit} AND ${_PurchaseModelSql.creditStatus} != ${_CreditStatus.payed} AND ${_PurchaseModelSql.customerName} LIKE '%${q}%' LIMIT ${limits} OFFSET ${skip}`;

  sql = `SELECT p.*,
    b.name AS "${_PurchaseModelSql.branchName}", b.type AS "${_PurchaseModelSql.branchType}",
    u.${UserModelSql.username} AS "${_PurchaseModelSql.adminName}", u.${UserModelSql.role} AS "${_PurchaseModelSql.adminRole}"
    FROM ${purchaseTable} p  
    LEFT JOIN ${BranchModelSql.branchTable} b ON p.${_PurchaseModelSql.branchId} = b.${BranchModelSql.id}
    LEFT JOIN ${UserModelSql.userTable} u ON p.${_PurchaseModelSql.adminId} = u.${UserModelSql.id}
    WHERE p.${_PurchaseModelSql.type} = ${_PurchaseType.credit} AND p.${_PurchaseModelSql.creditStatus} != ${_CreditStatus.payed} AND p.${_PurchaseModelSql.customerName} LIKE '%${q}%'
    LIMIT ${limits} OFFSET ${skip}`;

  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/getAllCredit', (req, res) => {
  const sql = `SELECT p.*,
  b.name AS "${_PurchaseModelSql.branchName}", b.type AS "${_PurchaseModelSql.branchType}",
  u.${UserModelSql.username} AS "${_PurchaseModelSql.adminName}", u.${UserModelSql.role} AS "${_PurchaseModelSql.adminRole}"
  FROM ${purchaseTable} p  
  LEFT JOIN ${BranchModelSql.branchTable} b ON p.${_PurchaseModelSql.branchId} = b.${BranchModelSql.id}
  LEFT JOIN ${UserModelSql.userTable} u ON p.${_PurchaseModelSql.adminId} = u.${UserModelSql.id}
  WHERE p.${_PurchaseModelSql.type} = "${_PurchaseType.credit}" 
  OR p.${_PurchaseModelSql.creditStatus} = "${_CreditStatus.unPayed}" 
  OR p.${_PurchaseModelSql.creditStatus} = "${_CreditStatus.advance}"`;
  stock_management.query(sql, (err, results) => {
    console.log(err);
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/getAllCreditsByDate', (req, res) => {
  const {startDateTime, endDateTime,chooseReportDateType} = req.body;
  let where = '';
  if(chooseReportDateType == 'ChooseReportDateType.thisWeek'){
    where = `${_PurchaseModelSql.createdAt} >= DATE(NOW()) - INTERVAL 7 DAY`;
  }else if(chooseReportDateType == 'ChooseReportDateType.thisMonth'){ 
    where = `MONTH(${_PurchaseModelSql.createdAt}) = MONTH(CURRENT_DATE()) AND YEAR(${_PurchaseModelSql.createdAt}) = YEAR(CURRENT_DATE())`;
  }else if(chooseReportDateType == 'ChooseReportDateType.thisYear'){ 
    where = `YEAR(${_PurchaseModelSql.createdAt}) = YEAR(CURDATE())`;
  } else {
    where = `${_PurchaseModelSql.createdAt} BETWEEN "${startDateTime}" AND "${endDateTime}"`;
  }
  const sql = `SELECT p.*,
  b.name AS "${_PurchaseModelSql.branchName}", b.type AS "${_PurchaseModelSql.branchType}",
  u.${UserModelSql.username} AS "${_PurchaseModelSql.adminName}", u.${UserModelSql.role} AS "${_PurchaseModelSql.adminRole}"
  FROM ${purchaseTable} p  
  LEFT JOIN ${BranchModelSql.branchTable} b ON p.${_PurchaseModelSql.branchId} = b.${BranchModelSql.id}
  LEFT JOIN ${UserModelSql.userTable} u ON p.${_PurchaseModelSql.adminId} = u.${UserModelSql.id}
  WHERE ${where}
  AND (p.${_PurchaseModelSql.type} = "${_PurchaseType.credit}" 
  OR p.${_PurchaseModelSql.creditStatus} = "${_CreditStatus.unPayed}" 
  OR p.${_PurchaseModelSql.creditStatus} = "${_CreditStatus.advance}") 
  `;
  stock_management.query(sql, (err, results) => {
    console.log(err);
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

module.exports = router;