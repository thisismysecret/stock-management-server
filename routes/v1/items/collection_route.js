/* @api /api/items */
const express = require('express');

const router = express.Router();
const { catchErrors } = require('../../../middleware/index');
const { isAuthenticated } = require('../../../middleware/isAuthenticated');
const {stock_management} = require('../../../core/db/mysql');
const Utils = require('../../../core/processor')
const CollectionModelSql = require('../../../models/collection_model');
const { optimizeQueryPagination } = require('../../../Global/routeHelpers');

const groupCollectionTable = 'groupcollection';
const markTable = 'mark';
const brandTable = 'brand';
const measurementTable = 'measurement';

router.post('/createCollectionTable', (req, res) => {
	const groupCollectionSql = `CREATE TABLE ${groupCollectionTable}
  (
    ${CollectionModelSql.id} INTEGER AUTO_INCREMENT PRIMARY KEY ,
    ${CollectionModelSql.name} TEXT
  )`;
  const markSql = `CREATE TABLE ${markTable}
  (
    ${CollectionModelSql.id} INTEGER AUTO_INCREMENT PRIMARY KEY ,
    ${CollectionModelSql.name} TEXT
  )`;
  const brandSql = `CREATE TABLE ${brandTable}
  (
    ${CollectionModelSql.id} INTEGER AUTO_INCREMENT PRIMARY KEY ,
    ${CollectionModelSql.name} TEXT
  )`;
  const measurementSql = `CREATE TABLE ${measurementTable}
  (
    ${CollectionModelSql.id} INTEGER AUTO_INCREMENT PRIMARY KEY ,
    ${CollectionModelSql.name} TEXT
  )`;
  stock_management.query(groupCollectionSql, (err, results) => {
    stock_management.query(markSql, (err, results) => {
      
        stock_management.query(brandSql, (err, results) => {
          stock_management.query(measurementSql, (err, results) => {
            return Utils.SuccessMsg(res, { });
          });
        });
    });
  
  });

		
});
router.post('/addNewItemToCollection', (req, res) => {
  const {tableName, name} = req.body;
  const sql = `INSERT INTO ${tableName}
  (
    ${CollectionModelSql.name}

  )
  VALUES
  (
    "${name}"
  )`
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

		
});

router.post('/updateCollection', (req, res) => {
  const {collectionModel, tableName} = req.body;
  const sql = `UPDATE ${tableName} SET 
  ${CollectionModelSql.name} = "${collectionModel.name}"
    WHERE ${CollectionModelSql.id} = ${collectionModel.id}`;
  stock_management.query(sql, (err, results) => {
    console.log(err);
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/getPaginatedCollectionItems', (req, res) => {
  const {tableName, page} = req.body;
  const {limits, skip} = optimizeQueryPagination(page)
  const sql = `SELECT * FROM ${tableName} LIMIT ${limits} OFFSET ${skip}`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/getAllCollectionItems', (req, res) => {
  const {tableName,} = req.body;
  const sql = `SELECT * FROM ${tableName}`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/deleteItemFromCollection', (req, res) => {
  const {id, tableName} = req.body;
  const sql = `DELETE FROM ${tableName} WHERE ${CollectionModelSql.id} = ${id}`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});


module.exports = router;