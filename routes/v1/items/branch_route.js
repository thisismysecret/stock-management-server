/* @api /api/items */
const express = require('express');

const router = express.Router();
const { catchErrors } = require('../../../middleware/index');
const { isAuthenticated } = require('../../../middleware/isAuthenticated');
const {stock_management} = require('../../../core/db/mysql');
const Utils = require('../../../core/processor')
const BranchModelSql = require('../../../models/branch_model');
const { optimizeQueryPagination } = require('../../../Global/routeHelpers');

const branchTable = 'branch'

router.post('/createBranchTable', (req, res) => {
	const sql = `CREATE TABLE ${branchTable}
  (
    ${BranchModelSql.id} INTEGER AUTO_INCREMENT PRIMARY KEY ,
    ${BranchModelSql.name} TEXT,
    ${BranchModelSql.address} TEXT,
    ${BranchModelSql.status} TEXT,
    ${BranchModelSql.type} TEXT
  )`;
 
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
    return Utils.SuccessMsg(res, { });
  });

		
});

router.post('/addNewBranchToDB', (req, res) => {
  const {branchModel} = req.body;
  const sql = `INSERT INTO ${branchTable}
  (
    ${BranchModelSql.name},
    ${BranchModelSql.address},
    ${BranchModelSql.status},
    ${BranchModelSql.type}

  )
  VALUES
  (
    "${branchModel.name}",
    "${branchModel.address}",
    "1",
    "${branchModel.type}"
  )`
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });
});

router.post('/getPaginatedBranches', (req, res) => {
  const {page, type} = req.body;
  const {limits, skip} = optimizeQueryPagination(page)
  let sql = ``;
  if(type && type.length > 0) {
    sql = `SELECT * FROM ${branchTable} WHERE ${BranchModelSql.type} = ${type} LIMIT ${limits} OFFSET ${skip}`;
  } else {
    sql = `SELECT * FROM ${branchTable} LIMIT ${limits} OFFSET ${skip}`;
  }
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});


router.post('/getAllBranches', (req, res) => {
  const {type} = req.body;
  let sql = ``;
  if(type && type.length > 0) {
    sql = `SELECT * FROM ${branchTable} WHERE ${BranchModelSql.type} = ${type}`;
  } else {
    sql = `SELECT * FROM ${branchTable} LIMIT ${limits} OFFSET ${skip}`;
  }
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/getBranchById', (req, res) => {
  const {id} = req.body;
  let sql = `SELECT * FROM ${branchTable} WHERE ${BranchModelSql.id} = ${id}`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/deleteBranchFromDB', (req, res) => {
  const {id} = req.body;
  const sql = `DELETE FROM ${branchTable} WHERE ${BranchModelSql.id} = ${id}`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/updateBranch', (req, res) => {
  const {branchModel} = req.body;
  const sql = `UPDATE ${branchTable} SET 
  ${BranchModelSql.name} = "${branchModel.name}",
  ${BranchModelSql.address} = "${branchModel.address}",
  ${BranchModelSql.type} = "${branchModel.type}"
    WHERE ${BranchModelSql.id} = ${branchModel.id}`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});


module.exports = router;