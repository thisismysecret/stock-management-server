/* @api /api/history */
const express = require('express');

const router = express.Router();
const { catchErrors } = require('../../../middleware/index');
const { isAuthenticated } = require('../../../middleware/isAuthenticated');
const {stock_management} = require('../../../core/db/mysql');
const Utils = require('../../../core/processor')
const HistoryModelSql = require('../../../models/history_model');
const UserModelSql = require('../../../models/user_model');
const { optimizeQueryPagination } = require('../../../Global/routeHelpers');

const historyTable = 'history';

router.post('/createHistoryTable', (req, res) => {
	const sql = `CREATE TABLE ${historyTable}
  (
    ${HistoryModelSql.id} INTEGER AUTO_INCREMENT PRIMARY KEY ,
    ${HistoryModelSql.type} TEXT,
    ${HistoryModelSql.result} TEXT,
    ${HistoryModelSql.item} TEXT,
    ${HistoryModelSql.createdAt} TEXT,
    ${HistoryModelSql.userId} INTEGER
  )`;
 
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
    return Utils.SuccessMsg(res, { });
  });

		
});

router.post('/addNewHistory', (req, res) => {
  const {historyModelData} = req.body;
  const sql = `INSERT INTO ${historyTable}
  (
    ${HistoryModelSql.type},
    ${HistoryModelSql.result},
    ${HistoryModelSql.item},
    ${HistoryModelSql.createdAt},
    ${HistoryModelSql.userId}
  )
  VALUES
  (
    "${historyModelData.type}",
    "${historyModelData.result}",
    "${historyModelData.item}",
    "${historyModelData.createdAt}",
    "${historyModelData.userId}"
  )`
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });
});

router.post('/getPaginatedHistory', (req, res) => {
  const {page, filterBy} = req.body;
  const {limits, skip} = optimizeQueryPagination(page)
  let sql = '';
  if(filterBy && filterBy.technicalName && filterBy.technicalName !== '' && filterBy.value) {
    // sql = `SELECT * FROM ${historyTable} LIMIT ${limits} OFFSET ${skip}`;
    sql =
    `SELECT h.*, u.${UserModelSql.username} AS "${HistoryModelSql.userName}", u.${UserModelSql.role} AS "${HistoryModelSql.userRole}" FROM ${historyTable} h
    LEFT JOIN ${UserModelSql.userTable} u ON h.${HistoryModelSql.userId} = u.${UserModelSql.id} 
    WHERE h.${filterBy.technicalName} ${filterBy.value}
    ORDER BY h.${HistoryModelSql.createdAt} DESC LIMIT ${limits} OFFSET ${skip}
    `;

  } else {
    sql =
    `SELECT h.*, u.${UserModelSql.username} AS "${HistoryModelSql.userName}", u.${UserModelSql.role} AS "${HistoryModelSql.userRole}" FROM ${historyTable} h
    LEFT JOIN ${UserModelSql.userTable} u ON h.${HistoryModelSql.userId} = u.${UserModelSql.id} 
    ORDER BY h.${HistoryModelSql.createdAt} DESC LIMIT ${limits} OFFSET ${skip}
    `;
  }
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});

router.post('/deleteHistory', (req, res) => {
  const {id} = req.body;
  const sql = `DELETE FROM ${historyTable} WHERE ${HistoryModelSql.id} = ${id}`;
  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
  });

});


module.exports = router;