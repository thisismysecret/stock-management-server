/* @api /api/items */
const express = require('express');

const router = express.Router();
const {stock_management} = require('../../../core/db/mysql');
const Utils = require('../../../core/processor')
const BillOfLadingModel = require('../../../models/bill_of_lading_model');
const { optimizeQueryPagination } = require('../../../Global/routeHelpers');

const billofladingTable = BillOfLadingModel.billofladingTable;

router.post('/createBillOfLadingTable', (req, res) => {
	const sql_q = `CREATE TABLE ${billofladingTable}
  (
    ${BillOfLadingModel.id} INTEGER AUTO_INCREMENT PRIMARY KEY ,
    ${BillOfLadingModel.cadNo} TEXT,
    ${BillOfLadingModel.createdAt} TEXT,
    ${BillOfLadingModel.blNo} TEXT,
    ${BillOfLadingModel.mscNo} TEXT,
    ${BillOfLadingModel.nm} TEXT,
    ${BillOfLadingModel.shippedOnBoard} TEXT
  )`;
    stock_management.query(sql_q, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
    if (results && results.length === 0) return Utils.ErrMsg(res, { msg: 'Failed to create' });
		return Utils.SuccessMsg(res, { });
	});
});

router.post('/addBillOfLading', (req, res) => {
  const {billOfLadingModel} = req.body;
	const sql = `INSERT INTO ${billofladingTable} 
  (
    ${BillOfLadingModel.cadNo},
    ${BillOfLadingModel.createdAt},
    ${BillOfLadingModel.blNo},
    ${BillOfLadingModel.mscNo},
    ${BillOfLadingModel.nm},
    ${BillOfLadingModel.shippedOnBoard}
  )
  VALUES 
  (
    "${billOfLadingModel.cadNo}",
    "${billOfLadingModel.createdAt}",
    "${billOfLadingModel.blNo}",
    "${billOfLadingModel.mscNo}",
    "${billOfLadingModel.nm}",
    "${billOfLadingModel.shippedOnBoard}"
  )
  `;
    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, { });
	});
});
router.post('/getPaginatedBillOfLading', (req, res) => {
  let {page} = req.body;

  let sql = '';

  const {limits, skip} = optimizeQueryPagination(page)

  sql = `SELECT * FROM ${billofladingTable} LIMIT ${limits} OFFSET ${skip}`;

  stock_management.query(sql, (err, results) => {
    console.log(err);
  if (err) return Utils.ErrMsg(res, {msg: err.code});
  return Utils.SuccessMsg(res, {data: results });
});
});
router.post('/getAllBillOfLading', (req, res) => {
  const sql = `SELECT * FROM ${billofladingTable}`;

    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});

router.post('/updateAllBillOfLading', (req, res) => {
  const {id, billOfLadingModel} = req.body;
  const sql = `UPDATE ${billofladingTable} SET 
  ${BillOfLadingModelSql.cadNo} = "${billOfLadingModel.cadNo}",
  ${BillOfLadingModelSql.blNo} = "${billOfLadingModel.blNo}",
  ${BillOfLadingModelSql.mscNo} = "${billOfLadingModel.mscNo}",
  ${BillOfLadingModelSql.nm} = "${billOfLadingModel.nm}",
  ${BillOfLadingModelSql.shippedOnBoard} = "${billOfLadingModel.shippedOnBoard}"
  WHERE ${BillOfLadingModelSql.id} = ${id}`;
    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});

router.post('/deleteExpense', (req, res) => {
  const {id} = req.body;
	const sql = `DELETE FROM ${billofladingTable} WHERE ${BillOfLadingModelSql.id} = ${id}`;
    stock_management.query(sql, (err, results) => {
      console.log(err);
		if (err) return Utils.ErrMsg(res, {msg: err.code});
		return Utils.SuccessMsg(res, {data: results });
	});
});

module.exports = router;