/* @api /api/history */
const express = require('express');

const router = express.Router();
const { catchErrors } = require('../../../middleware/index');
const { isAuthenticated } = require('../../../middleware/isAuthenticated');
const {stock_management} = require('../../../core/db/mysql');
const Utils = require('../../../core/processor')
const HistoryModelSql = require('../../../models/history_model');
const UserModelSql = require('../../../models/user_model');
const { optimizeQueryPagination } = require('../../../Global/routeHelpers');
const { _PurchaseModelSql } = require('../../../models/purchase_model');
const item_model = require('../../../models/item_model');
const storeItemTable = require('../../../models/item_model');
const branch_model = require('../../../models/branch_model');

// SUM(s.${item_model.qty} + i.${item_model.qty}) AS "totalQty",
router.post('/getStockAndStoreItem', (req, res) => {
  let sql =  `SELECT 
  s.qty AS "storeQty",
  i.*
  FROM ${item_model.itemTable} i 
  LEFT JOIN ${item_model.storeItemTable} s ON s.${item_model.stockItemId} = i.${item_model.id}
  `;
  
  stock_management.query(sql, (err, results) => {
    console.log(err);
    if (err) return Utils.ErrMsg(res, {msg: err.code});
    return Utils.SuccessMsg(res, {data: results });
  });
});


router.post('/getTotalRevenue', (req, res) => {
  const {dateBtn} = req.body;
  console.log(req.body);
  let sql = '';
  if(dateBtn) {
    sql = `SELECT SUM(${_PurchaseModelSql.totalAmount} - ${_PurchaseModelSql.unpaidAmount}) AS totalAmount 
    FROM ${_PurchaseModelSql.purchaseTable} 
    WHERE ${_PurchaseModelSql.createdAt} BETWEEN "${dateBtn[0]}" AND "${dateBtn[1]}"
    LIMIT 1
    `;
  } else {
    sql = `SELECT SUM(${_PurchaseModelSql.totalAmount} - ${_PurchaseModelSql.unpaidAmount}) AS totalAmount FROM ${_PurchaseModelSql.purchaseTable} LIMIT 1`;
  }

  stock_management.query(sql, (err, results) => {
    console.log(err);
    if (err) return Utils.ErrMsg(res, {msg: err.code});
    console.log(results);
    return Utils.SuccessMsg(res, {data: results });
  });
});

router.post('/getTotalRevenueByBranch', (req, res) => {
  const {branchId} = req.body;
  const sql = `SELECT SUM(${_PurchaseModelSql.totalAmount} - ${_PurchaseModelSql.unpaidAmount}) AS totalAmount FROM ${_PurchaseModelSql.purchaseTable}
  WHERE ${_PurchaseModelSql.branchId} = ${branchId}
  LIMIT 1`;

  stock_management.query(sql, (err, results) => {
    if (err) return Utils.ErrMsg(res, {msg: err.code});
    return Utils.SuccessMsg(res, {data: results });
  });
});


router.post('/getTotalRevenueAllBranches', (req, res) => {
  const {branchId} = req.body;
  let branchesList = [];
  let branchSql = `SELECT * FROM ${branch_model.branchTable}`;

  stock_management.promise().query(branchSql).then( ([rows, fields]) => {
    branchesList = rows
    const data = [];
    for (let index = 0; index < branchesList.length; index++) {
      const branch = branchesList[index];
      const sql = `SELECT SUM(${_PurchaseModelSql.totalAmount} - ${_PurchaseModelSql.unpaidAmount}) AS totalAmount,
      items->'$.items[*].qty'
      FROM ${_PurchaseModelSql.purchaseTable}
      WHERE ${_PurchaseModelSql.branchId} = ${branch.id}
      LIMIT 1`;
      stock_management.promise().query(sql).then(([rows, fields]) => {
          data.push({
            // branchId: branch.id, totalAmount : rows[0]['totalAmount'] || 0,
            branchId: branch.id, totalAmount: rows,
          })
      })
  
     
    }
    return Utils.SuccessMsg(res, {data });
  });

  

  

  // const sql = `SELECT SUM(${_PurchaseModelSql.totalAmount} - ${_PurchaseModelSql.unpaidAmount}) AS totalAmount FROM ${_PurchaseModelSql.purchaseTable}
  // WHERE ${_PurchaseModelSql.branchId} = ${branchId}
  // LIMIT 1`;

  // stock_management.query(sql, (err, results) => {
  //   if (err) return Utils.ErrMsg(res, {msg: err.code});
  //   return Utils.SuccessMsg(res, {data: results });
  // });
});



router.post('/getAllProductTotal', (req, res) => {

  let branchesList = [];
  let stockBranches = [];
  let stockBranchesIds = [];
  let storeBranches = [];
  let storeBranchesIds = [];

  let queryLengths = [];
  let sql = [];

  // get all branch
  let branchSql = `SELECT * FROM ${branch_model.branchTable}`;
  stock_management.promise().query(branchSql).then(([rows, fields]) => {
    console.log(rows);
    branchesList = rows



    // filter branch by type
    if(branchesList && branchesList.length > 0) {
      branchesList.forEach(element => {
        if(element.type == branch_model.stock) {
          stockBranches.push(element)
          stockBranchesIds.push(element.id)
        } else if(element.type == branch_model.store){
          storeBranches.push(element)
          storeBranchesIds.push(element.id)
        }
      });
    }


    // add query by specific branch
    if(stockBranches && stockBranches.length > 0){
      stockBranches.forEach((element, index) => {
        sql.push(
          `SELECT 
          ${item_model.branchId},
          SUM(${item_model.unitPrice} * ${item_model.qty}) AS "stockTotalUnitPrice", 
          SUM(${item_model.qty}) AS "stockTotalQty", 
          COUNT(*) AS "stockTotalItems" 
          FROM ${item_model.itemTable} 
          WHERE ${item_model.branchId} = ${element.id}
          LIMIT 1
          `
        )
        queryLengths.push(index)
      });
    }

    if(storeBranches && storeBranches.length > 0){
      storeBranches.forEach((element, index) => {
        sql.push(
          `SELECT
          s.${item_model.branchId},
          SUM(i.${item_model.unitPrice} * s.${item_model.qty}) AS "storeTotalUnitPrice", 
          SUM(s.${item_model.qty}) AS "storeTotalQty", 
          COUNT(*) AS "storeTotalItems" 
          FROM ${storeItemTable.storeItemTable} s 
          INNER JOIN ${item_model.itemTable} i ON s.${item_model.stockItemId} = i.${item_model.id}
          WHERE s.${item_model.branchId} = ${element.id}
          LIMIT 1
          `
        )
        queryLengths.push(stockBranches.length + index)
      });
    }

    stock_management.query(sql.join(';').toString(), queryLengths, (err, results) => {
      if (err) {
        console.log(err);
        return Utils.ErrMsg(res, {msg: err.code});
      }
      const stocks = []
      const stores = []
      results.forEach((v) => {
        if(stockBranchesIds.indexOf(v[0].branchId) > -1) {
          console.log('stock');
          v[0][item_model.branchName] = stockBranches.filter((b) => b.id === v[0][item_model.branchId])[0][branch_model.name]
          stocks.push(...v);
        }

        if(storeBranchesIds.indexOf(v[0].branchId) > -1) {
          console.log('store');
          v[0][item_model.branchName] = storeBranches.filter((b) => b.id === v[0][item_model.branchId])[0][branch_model.name]
          stores.push(...v);
        }

      });
      return Utils.SuccessMsg(res, {data: {stocks ,stores} });
    });
  })





  // const sql = `
  // SELECT 
  // SUM(${item_model.unitPrice} * ${item_model.qty}) AS "stockTotalUnitPrice", 
  // SUM(${item_model.qty}) AS "stockTotalQty", 
  // COUNT(*) AS "stockTotalItems" 
  // FROM ${item_model.itemTable} LIMIT 1;

  // SELECT 
  // SUM(i.${item_model.unitPrice} * s.${item_model.qty}) AS "storeTotalUnitPrice", 
  // SUM(s.${item_model.qty}) AS "storeTotalQty", 
  // COUNT(*) AS "storeTotalItems" 
  // FROM ${storeItemTable.storeItemTable} s 
  // INNER JOIN ${item_model.itemTable} i ON s.${item_model.stockItemId} = i.${item_model.id} 
  // LIMIT 1;

  // `;

  
  
});



module.exports = router;