
class PurchaseModelSql {
  constructor() {
    this.id = 'id';
this.items = 'items';
this.createdAt = 'createdAt';
this.updatedAt = 'updatedAt';
this.customerId = 'customerId';
this.adminId = 'adminId';
this.customerPhoneNo = 'customerPhoneNo';
this.type = 'type';
this.customerName = 'customerName';
this.returnDate = 'returnDate';
this.creditStatus = 'creditStatus';
this.creditPaymentAmountHistory = 'creditPaymentAmountHistory';
this.totalAmount = 'totalAmount';
this.storeTotal = 'storeTotal';
this.stockTotal = 'stockTotal';
this.branchId = 'branchId';
this.branchName = 'branchName';
this.adminName = 'adminName';
this.adminRole = 'adminRole';
this.unpaidAmount = 'unpaidAmount';

// table

this.purchaseTable = 'purchase';

// items
this.purchaseId = 'purchaseId';
this.qty = 'qty';
this.amount = 'amount';
this.itemId = 'itemId';
this.storeId = 'storeId';
this.branchType = 'branchType';
this.customItemIdentifer = 'customItemIdentifer';
  }
}

class PurchaseType {
  constructor() {
    this.cash = '0';
  this.credit = '1';
  this.cheque = '2';
  }
}

class CreditStatus {
  constructor() {
    this.unPayed = '0';
  this.advance = '1';
  this.payed = '2';
  }
}

const _PurchaseModelSql = new PurchaseModelSql()
const _PurchaseType = new PurchaseType()
const _CreditStatus = new CreditStatus()

module.exports = {_PurchaseModelSql,
  _PurchaseType,
  _CreditStatus};