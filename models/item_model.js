class ItemModelSql {
  constructor() {
    this.id = 'id';
    this.originalId = 'oId';
    this.image = 'image';
    this.name = 'name';
    this.code = 'code';
    this.invoiceNo = 'invoiceNo';
    this.invoiceDate = 'invoiceDate';
    this.partNo = 'partNo';
    this.barcode = 'barcode';
    this.unitPrice = 'unitPrice';
    this.minPrice = 'minPrice';
    this.maxPrice = 'maxPrice';
    this.qty = 'qty';
    this.adminId = 'adminId';
    this.location = 'location';
    this.groupp = 'groupp';
    this.mark = 'mark';
    this.description = 'description';
    this.branchId = 'branchId';
    this.branchName = 'branchName';
    this.branchType = 'branchType';
    this.manufacturer = 'manufacturer';
    this.manufacturerDate = 'manufacturerDate';
    this.brand = 'brand';
    this.measurement = 'measurement';
    this.createdAt = 'createdAt';
    this.status = 'status';
    this.stockItemId = 'stockItemId';
    this.stockOriginalItemId = 'sOIId';
    this.purchasePrice = 'purchasePrice';
    this.expireDate = 'expireDate';
    this.expireRemindId = 'expireRemindId';
    

    // tables
    this.storeItemTable = 'storeItems';
    this.itemTable = 'items';
  }
}

module.exports = new ItemModelSql();