class ExpenseModelSql {
  constructor() {
    this.id = 'id';
    this.date = 'date';
    this.payee = 'payee';
    this.note = 'note';
    this.amount = 'amount';
    
    // tables
    this.expenseTable = 'expense';
  }
}

module.exports = new ExpenseModelSql();