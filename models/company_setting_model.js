class CompanySettingModelSql {
  constructor() {
    this.id = 'id';
    this.name = 'name';
    this.logo = 'logo';
    this.isOnline = 'isOnline';
    this.address = 'address';
    this.companyDomain = 'companyDomain';
    this.companyPassword = 'companyPassword';
    this.serverUrl = 'serverUrl';
  }
}


module.exports = new CompanySettingModelSql();