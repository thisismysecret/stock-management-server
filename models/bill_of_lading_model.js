class BillOfLadingModel {
  constructor() {
    this.id = 'id';
    this.createdAt = 'createdAt';
    this.cadNo = 'cadNo';
    this.blNo = 'blNo';
    this.mscNo = 'mscNo';
    this.nm = 'nm';
    this.shippedOnBoard = 'shippedOnBoard';
    
    // tables
    this.billofladingTable = 'billoflading';
  }
}

module.exports = new BillOfLadingModel();