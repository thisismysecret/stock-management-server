class MyCreditModelSql {
  constructor(){
    this.id = 'id';
    this.items = 'items';
    this.createdAt = 'createdAt';
    this.updatedAt = 'updatedAt';
    this.companyIdentifer = 'companyIdentifer';
    this.adminId = 'adminId';
    this.creditStatus = 'creditStatus';
    this.creditPaymentAmountHistory = 'creditPaymentAmountHistory';
  }
}

module.exports = new MyCreditModelSql();