class HistoryModelSql {
  constructor() {
    this.id = 'id';
    this.result = 'result';
    this.type = 'type';
    this.item = 'item';
    this.createdAt = 'createdAt';
    this.userId = 'userId';    
    this.userName = 'userName';    
    this.userRole = 'userRole';    
  }
}


module.exports = new HistoryModelSql();