class BranchLocalSql {
  constructor() {
    this.id = 'id';
    this.name = 'name';
    this.address = 'address';
    this.status = 'status';
    this.type = 'type';
    this.branchTable = 'branch';

    this.store = '0';
    this.stock = '1';
  }
  
}

module.exports = new BranchLocalSql();