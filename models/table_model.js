class TableModel {
  constructor() {
    this.itemsTable = 'items';
    this.storeItemsTable = 'storeItems';
    this.groupcollectionTable = 'groupcollection';
    this.markTable = 'mark';
    this.brandTable = 'brand';
    this.measurementTable = 'measurement';
    this.userTable = 'user';
    this.purchaseTable = 'purchase';
    this.historyTable = 'history';
    this.companysettingTable = 'companysetting';
    this.customerTable = 'customer';
    this.billofladingTable = 'billoflading';
    this.branchTable = 'branch';
    this.expenseTable = 'expense';
    this.mycreditsTable = 'mycredits';
  }
}

module.exports = new TableModel();