class UserModelSql {
  constructor() {
    this.id = 'id';
    this.username = 'username';
    this.avatar = 'avatar';
    this.role = 'role';
    this.phone = 'phone';
    this.password = 'password';
    this.salesBranchId = 'salesBranchId';
    this.userTable = 'user';
  }
}


module.exports = new UserModelSql();