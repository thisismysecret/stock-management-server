
const itemsApiRoutes = require('../routes/v1/items/items_route');
const branchApiRoutes = require('../routes/v1/items/branch_route');
const historyApiRoutes = require('../routes/v1/history/history_route');
const collectionApiRoutes = require('../routes/v1/items/collection_route');
const purchaseApiRoutes = require('../routes/v1/purchase/purchase_route');
const creditApiRoutes = require('../routes/v1/credit/credit_route');
const customerApiRoutes = require('../routes/v1/credit/customer_route');
const userApiRoutes = require('../routes/v1/user/user_route');
const myCreditApiRoutes = require('../routes/v1/user/my_credit_route');
const companySettingApiRoutes = require('../routes/v1/user/company_setting_route');
const financeApiRoutes = require('../routes/v1/finance/finance_route');
const storeItemsApiRoutes = require('../routes/v1/items/store_items_route');
const expenseApiRoutes = require('../routes/v1/user/expense_route');
const billOfLadingApiRoutes = require('../routes/v1/billOfLading/bill_of_lading');
const generalSettingApiRoutes = require('../routes/v1/user/general_setting_route');


module.exports = (app) => {
	app.use('/items', itemsApiRoutes);
	app.use('/items', branchApiRoutes);
	app.use('/items', collectionApiRoutes);

	app.use('/store_items', storeItemsApiRoutes);

	app.use('/history', historyApiRoutes);

	app.use('/purchase', purchaseApiRoutes);
	app.use('/purchase', creditApiRoutes);

	app.use('/customer', customerApiRoutes);

	app.use('/user', userApiRoutes);
	app.use('/user', myCreditApiRoutes);
	app.use('/user', companySettingApiRoutes);
	app.use('/user/expense', expenseApiRoutes);

	app.use('/finance', financeApiRoutes);
	app.use('/billoflading', billOfLadingApiRoutes);

	app.use('/general', generalSettingApiRoutes);
};
