// const mysql = require('mysql2');
const mysql = require('mysql2');
const Sequelize = require('sequelize');
const ItemModelSql = require('../../models/item_model');

const stock_management = mysql.createPool({
	host: process.env.DB_HOST,
	user: process.env.DB_USER,
	port: '3306',
	password: process.env.DB_PASSWORD,
	database: process.env.DB_DB,
	multipleStatements: true,
	waitForConnections: true,
  connectionLimit: 10,

});

const yeneStockDb = new Sequelize(process.env.DB_DB, process.env.DB_USER, process.env.DB_PASSWORD, {
	host: process.env.DB_HOST,
	dialect: 'mysql',
	pool: {
		max: 5,
		min: 0,
		acquire: 30000,
		idle: 10000,
	},
});

// stock_management.connect();
 
module.exports = {
	stock_management,
	yeneStockDb,
};
