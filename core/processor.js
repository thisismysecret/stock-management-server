class Utils {
	ErrMsg(res, option = {
		Code, msg, status, subCode,err
	}) {
		res.status(option.status ? option.status : 200).send({
			Code: option.Code ? option.Code : '-1',
			status: option.status ? option.status : 200,
			Msg: option.msg ? option.msg : 'Parameter error',
			Data: null,
			subCode: option.subCode ? option.subCode : null,
			err: option.err ? option.err : null,
		});
	}

	SuccessMsg(res, option = {
		msg, data, total, subCode,
	}) {
		res.status(option.status ? option.status : 200).send({
			Code: 0,
			Msg: option.msg ? option.msg : 'ok',
			status: option.status ? option.status : 200,
			Data: (option.data !== null ||option.data !== undefined) ? option.data : [],
			total: option.total ? option.total : null,
			subCode: option.subCode ? option.subCode : null,
		});
	}

	EmptyObj(res, option = {
		Code, msg, total, status, subCode,
	}) {
		return res.status(option.status ? option.status : 200).send({
			Code: option.Code ? option.Code : '--2',
			status: option.status ? option.status : 200,
			Msg: option.msg ? option.msg : '',
			Data: option.data ? option.data : 'NO_CONTENT',
			total: option.total ? option.total : 0,
			subCode: option.subCode ? option.subCode : null,
		});
	}

	ReturnMessage(res, data) {
		if (data) {
			return this.SuccessMsg(res, {});
		}
		return this.ErrMsg(res, { msg: 'The operation has been failed please try again' });
	}
}

module.exports = new Utils();


/*

Info: true
Message: "Thanks for the feedback! Votes cast by those with less than 15 <a href="/help/whats-reputation">reputation</a> are recorded, but do not change the publicly displayed post score."
NewScore: 4
Refresh: false
Success: false
Transient: false
Warning: false


 */
